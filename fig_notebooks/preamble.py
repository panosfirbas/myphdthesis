# THIS FILE IS COPIED AS A SYMBOLIC LINK
# IN THE $HOME/.ipython/profile_default/startup/
# FOLDER. THIS WAY WHATEVER EXISTS IN THIS PYTHON FILE
# IS FOUND IN ALL NOTEBOOKS

import matplotlib
matplotlib.use("pdf")
%matplotlib inline
from matplotlib import pyplot as plt

# Some custom parameters, 
# this sets up the figure sizes and the fonts perfectly
plt.rc('font', family='monospace')
plt.rc('pdf', fonttype=42)
plt.rc('text', usetex=False)
plt.rc('xtick', labelsize=8)
plt.rc('ytick', labelsize=8)
plt.rc('axes', labelsize=9)
plt.rc('savefig',dpi=500)
plt.rc('figure',dpi=100)
plt.rc('legend', fontsize=9)

# for a B5 page
THESIS_PAGEWIDTH = 5.12 #6.9

import seaborn as sns
from pybedtools import BedTool as BT
import pandas as pd
import numpy as np

#STAT things
from scipy.stats import mannwhitneyu as MWU
from math import log
from sklearn import preprocessing
from scipy.stats import pearsonr
from scipy.stats import hypergeom
statf = hypergeom.sf

from sklearn.preprocessing import scale, normalize, robust_scale
from sklearn.metrics.pairwise import pairwise_distances as PWD
import scipy.spatial as sp, scipy.cluster.hierarchy as hc


#Other imports
from collections import Counter
import gzip
import re
from glob import glob
import pickle
# 
# import warnings
# warnings.filterwarnings('ignore')


# These are masking files used to filter out peaks that fall in regions of the genome that are masked as repeats
zebrafish_mask = BT("/home/ska/panos/myphdthesis/data/genomes/RepMask_danRer10.bed.gz")
amphi_mask = BT("/home/ska/panos/myphdthesis/data/genomes/RepMask_bl71nemr.bed.gz")
mouse_mask = BT("/home/ska/panos/myphdthesis/data/genomes/RepMask_mm10.bed.gz")
medaka_mask = BT("/home/ska/panos/myphdthesis/data/genomes/RepMask_orylat2.bed.gz")

# genome sizes
zebrafish_effective = 654384100
amphi_effective = 327521540
mouse_effective = 1456094557
medaka_effective = 677265199

#ATACseq peaks
amphi_idr = lambda s :"/home/ska/panos/myphdthesis/data/atac_peaks/amphi_{}_idrpeaks.bed.gz".format(s)
zebra_idr = lambda s :"/home/ska/panos/myphdthesis/data/atac_peaks/zebra_danRer10_{}_idrpeaks.bed.gz".format(s)
medaka_idr = lambda s :"/home/ska/panos/myphdthesis/data/atac_peaks/medaka_{}_idrpeaks.bed.gz".format(s)
mouse_idr = lambda s :"/home/ska/panos/myphdthesis/data/atac_peaks/mouse_{}_idrpeaks.bed.gz".format(s)

# CHIPSEQ peaks
amphi_k27_005combo = lambda s :"/home/ska/panos/myphdthesis/data/CHIP/amphi/p005_peaks/H3K27ac_{}_combo_peaks.narrowPeak.gz".format(s)
amphi_k4_005combo = lambda s :"/home/ska/panos/myphdthesis/data/CHIP/amphi/p005_peaks/H3K4me3_{}_combo_peaks.narrowPeak.gz".format(s)
amphi_k27_001combo = lambda s :"/home/ska/panos/myphdthesis/data/CHIP/amphi/p001_peaks/H3K27ac_{}_combo_loose_peaks.narrowPeak.gz".format(s)
amphi_k4_001combo = lambda s :"/home/ska/panos/myphdthesis/data/CHIP/amphi/p001_peaks/H3K4me3_{}_combo_loose_peaks.narrowPeak.gz".format(s)
zebra_k27_005combo = lambda s :"/home/ska/panos/myphdthesis/data/CHIP/zebra/p005_peaks/H3K27ac_{}_combo_peaks.narrowPeak.gz".format(s)
zebra_k4_005combo = lambda s :"/home/ska/panos/myphdthesis/data/CHIP/zebra/p005_peaks/H3K4me3_{}_combo_peaks.narrowPeak.gz".format(s)
zebra_k27_001combo = lambda s :"/home/ska/panos/myphdthesis/data/CHIP/zebra/p001_peaks/H3K27ac_{}_combo_loose_peaks.narrowPeak.gz".format(s)
zebra_k4_001combo = lambda s :"/home/ska/panos/myphdthesis/data/CHIP/zebra/p001_peaks/H3K4me3_{}_combo_loose_peaks.narrowPeak.gz".format(s)

#Gene REGions
gr_great = {}
gr_great['Dre'] = pd.read_csv("/home/ska/panos/myphdthesis/data/genomic_regions/GREAT_dre.bed.gz", sep='\t', header=None)
gr_great['Dre'].columns = ['chrom','start','end','geneID','score','strand']
gr_great['Dre']['score'] = gr_great['Dre']['end'] - gr_great['Dre']['start']
gr_great['Bla'] = pd.read_csv("/home/ska/panos/myphdthesis/data/genomic_regions/GREAT_bla.bed.gz", sep='\t', header=None)
gr_great['Bla'].columns = ['chrom','start','end','geneID','score','strand']
gr_great['Bla']['score'] = gr_great['Bla']['end'] - gr_great['Bla']['start']
gr_great['Ola'] = pd.read_csv("/home/ska/panos/myphdthesis/data/genomic_regions/GREAT_ola.bed.gz", sep='\t', header=None)
gr_great['Ola'].columns = ['chrom','start','end','geneID','score','strand']
gr_great['Ola']['score'] = gr_great['Ola']['end'] - gr_great['Ola']['start']
gr_great['Mmu'] = pd.read_csv("/home/ska/panos/myphdthesis/data/genomic_regions/GREAT_mmu.bed.gz", sep='\t', header=None)
gr_great['Mmu'].columns = ['chrom','start','end','geneID','score','strand']
gr_great['Mmu']['score'] = gr_great['Mmu']['end'] - gr_great['Mmu']['start']

#Gene REGions
gr_basal = {}
gr_basal['Dre'] = pd.read_csv("/home/ska/panos/myphdthesis/data/genomic_regions/BASAL_dre.bed.gz", sep='\t', header=None)
gr_basal['Dre'].columns = ['chrom','start','end','geneID','strand']
gr_basal['Dre']['score'] = gr_basal['Dre']['end'] - gr_basal['Dre']['start']
gr_basal['Dre'] = gr_basal['Dre'][['chrom','start','end','geneID','score','strand']]
gr_basal['Bla'] = pd.read_csv("/home/ska/panos/myphdthesis/data/genomic_regions/BASAL_bla.bed.gz", sep='\t', header=None)
gr_basal['Bla'].columns = ['chrom','start','end','geneID','strand']
gr_basal['Bla']['score'] = gr_basal['Bla']['end'] - gr_basal['Bla']['start']
gr_basal['Bla'] = gr_basal['Bla'][['chrom','start','end','geneID','score','strand']]
gr_basal['Ola'] = pd.read_csv("/home/ska/panos/myphdthesis/data/genomic_regions/BASAL_ola.bed.gz", sep='\t', header=None)
gr_basal['Ola'].columns = ['chrom','start','end','geneID','strand']
gr_basal['Ola']['score'] = gr_basal['Ola']['end'] - gr_basal['Ola']['start']
gr_basal['Ola'] = gr_basal['Ola'][['chrom','start','end','geneID','score','strand']]
gr_basal['Mmu'] = pd.read_csv("/home/ska/panos/myphdthesis/data/genomic_regions/BASAL_mmu.bed.gz", sep='\t', header=None)
gr_basal['Mmu'].columns = ['chrom','start','end','geneID','strand']
gr_basal['Mmu']['score'] = gr_basal['Mmu']['end'] - gr_basal['Mmu']['start']
gr_basal['Mmu'] = gr_basal['Mmu'][['chrom','start','end','geneID','score','strand']]

# Homologous gene families
### Grouping of genes into homologous-families       

# We have in our disposal a precomputed table where genes of various species
# are separated into homologous families.

# Each row is a family, each column a species. 
# The paralogues of each species are separated with ":" so when we load this dataset
# we split the strings into lists.

# We also create a second dataframe "genefamsC" which has the same index and shape as the first one,
# but contains the count of genes in each cell.
genefams = pd.read_csv("/home/ska/panos/myphdthesis/data/gene_categories/ortho_gene_families.tsv",
                      sep='\t')
genefams = genefams.applymap(lambda x: x.split(":") if x==x else x)
genefamsC = genefams.applymap(lambda x: len(x) if x==x else 0)

# the table needs a little improving:    
# some families of multiple proteins are identical,
# but their IDs are in different order, so they can't be recognized 
# as identical.we'll go through the table and sort all name with 
# multiple IDs in order to make identical families have identical names.
def order_things(x):
    return ';'.join(sorted(x.split(';')))

superfams = pd.read_csv("/home/ska/panos/myphdthesis/data/PWM/PWMname_to_ProteinFamily.tsv.gz", sep='\t', header=None)

superfams.columns = ['PWMname','GFid','GFname']
superfams.head()

lot = []
for i,row in superfams.iterrows():
    if ';' not in row['GFid']:
        lot.append( row.tolist() )
    else:
        unholy = zip(*sorted(
                zip(row['GFid'].split(';'),
                    row['GFname'].split(';')),
                    key= lambda x: int(x[0]) ))
        a,b = [';'.join(jj) for jj in unholy]
        lot.append( [row['PWMname'], a, b] )
        
superfams_ = pd.DataFrame(lot)

# SFD here, maps the PWM names to unique protein numbers:
# 'AP-2_Average_15': '17068'
SFD = dict(superfams_[[0,1]].to_records(index=False))

# but then in cases like 'ARID_BRIGHT_RFX_Average_2',
# we have multiple proteins: 66;15742;15743;18247
# we will replace this long concatenation of numbers 
# with a unique number:
fuid = {}
c = 0
for v in SFD.values():
    if v not in fuid:
        fuid[v] = c
        c += 1
SFDu = {k:fuid[v] for k,v in SFD.items()}




def boxplot_pvals(data, x,x_anq,x_targs,y,hue,hue_order):
    for targetx in x_targs:
        for huel in hue_order:
            m1 = (data[x]==x_anq)
            mh = data[hue] == huel
            m2 = (data[x]==targetx)
            print(targetx, huel, MWU( data.loc[mh & m2, y].values, data.loc[mh & m1, y].values, alternative='greater' ).pvalue)

def boxplot_pvals_nohue(data, x,x_anq,x_targs,y):
    for targetx in x_targs:
            m1 = (data[x]==x_anq)
            m2 = (data[x]==targetx)
            print(targetx, MWU( data.loc[m2, y].values, data.loc[ m1, y].values, alternative='greater' ).pvalue)