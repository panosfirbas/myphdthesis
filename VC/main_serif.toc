\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\contentsline {chapter}{Declaration of Authorship}{v}{section*.2}
\contentsline {chapter}{Resumen}{ix}{section*.3}
\contentsline {chapter}{Abstract}{xi}{section*.4}
\contentsline {chapter}{Acknowledgements}{xiii}{section*.5}
\contentsline {chapter}{Foreword: A few words on the Thesis itself}{xvi}{section*.6}
\contentsline {chapter}{ Repository - Contact Info}{xvii}{section*.6}
\contentsline {chapter}{Contents}{xix}{chapter*.9}
\contentsline {part}{I\hspace {1em}Introduction}{1}{part.10}
\contentsline {section}{\numberline {0.1}Transcription Regulation}{5}{section.13}
\contentsline {chapter}{\numberline {1}Trans-Regulation}{7}{chapter.15}
\contentsline {section}{\numberline {1.1}DNA-protein Binding}{7}{section.16}
\contentsline {section}{\numberline {1.2}Wet lab techniques}{9}{section.20}
\contentsline {section}{\numberline {1.3}Dry lab techniques}{11}{section.25}
\contentsline {subsection}{\numberline {1.3.1}PWMs}{11}{subsection.26}
\contentsline {subsection}{\numberline {1.3.2}Other seq-based approaches}{12}{subsection.28}
\contentsline {section}{\numberline {1.4}Neural Networks}{12}{section.29}
\contentsline {subsection}{\numberline {1.4.1}How NNs work}{13}{subsection.31}
\contentsline {section}{\numberline {1.5}Other non seq-based approaches}{18}{section.38}
\contentsline {section}{\numberline {1.6}Nimrod}{19}{section.41}
\contentsline {chapter}{\numberline {2}Cis-Regulation}{21}{chapter.43}
\contentsline {section}{\numberline {2.1}Cis-Regulation}{21}{section.44}
\contentsline {subsection}{\numberline {2.1.1}Promoters}{22}{subsection.45}
\contentsline {subsection}{\numberline {2.1.2}Enhancers}{22}{subsection.47}
\contentsline {section}{\numberline {2.2}Chromatin accessibility}{23}{section.48}
\contentsline {subsection}{\numberline {2.2.1}ATAC-seq}{24}{subsection.50}
\contentsline {section}{\numberline {2.3}Histone modifications}{28}{section.55}
\contentsline {subsection}{\numberline {2.3.1}H3K4me3}{29}{subsection.57}
\contentsline {subsection}{\numberline {2.3.2}H3K27ac}{30}{subsection.58}
\contentsline {section}{\numberline {2.4}Under the light of Evolution}{31}{section.59}
\contentsline {subsection}{\numberline {2.4.1} on the Tree of life }{32}{subsection.60}
\contentsline {subsection}{\numberline {2.4.2}Amphioxus}{34}{subsection.64}
\contentsline {subsection}{\numberline {2.4.3}Whole Genome Duplications}{34}{subsection.65}
\contentsline {chapter}{\numberline {3} Objectives }{37}{chapter.67}
\contentsline {part}{II\hspace {1em}Results: The origins of vertebrate gene regulation}{39}{part.73}
\contentsline {chapter}{\numberline {4} Introductory Analyses}{41}{chapter.74}
\contentsline {section}{\numberline {4.1}The genomes}{41}{section.75}
\contentsline {section}{\numberline {4.2}Intergenic regions}{43}{section.77}
\contentsline {section}{\numberline {4.3}GREAT regions}{44}{section.80}
\contentsline {section}{\numberline {4.4}Histone Modification ChIP-seq}{45}{section.82}
\contentsline {subsection}{\numberline {4.4.1}Width of peaks}{46}{subsection.83}
\contentsline {subsection}{\numberline {4.4.2}Number of peaks/ Genome coverage}{46}{subsection.85}
\contentsline {section}{\numberline {4.5}ATAC-seq}{50}{section.89}
\contentsline {section}{\numberline {4.6}CRE-TSS distances}{51}{section.91}
\contentsline {section}{\numberline {4.7}Higher regulatory content }{53}{section.94}
\contentsline {subsection}{\numberline {4.7.1}Matched genomic region sizes}{54}{subsection.96}
\contentsline {subsection}{\numberline {4.7.2}Downsampling}{56}{subsection.99}
\contentsline {chapter}{\numberline {5} Conservation of cis regulation }{57}{chapter.101}
\contentsline {section}{\numberline {5.1} NACC }{57}{section.102}
\contentsline {section}{\numberline {5.2}The phylotypic period}{59}{section.105}
\contentsline {section}{\numberline {5.3} Gene Modules }{62}{section.109}
\contentsline {subsection}{\numberline {5.3.1}The WGCNA analysis}{62}{subsection.110}
\contentsline {subsection}{\numberline {5.3.2} Homologous Gene Content }{63}{subsection.112}
\contentsline {subsection}{\numberline {5.3.3} Cis-Regulatory Content }{64}{subsection.114}
\contentsline {chapter}{\numberline {6} Regulatory content and gene fate after WGD }{69}{chapter.118}
\contentsline {subsection}{\numberline {6.0.1} Gene Fate after WGD }{69}{subsection.119}
\contentsline {subsection}{\numberline {6.0.2} Daltons per 1-X max }{71}{subsection.122}
\contentsline {subsection}{\numberline {6.0.3} Increased regulatory complexity in functionally specialized ohnologs }{72}{subsection.125}
\contentsline {part}{III\hspace {1em}Results: Detecting TF binding events with a Neural Network}{79}{part.131}
\contentsline {section}{\numberline {6.1}Training concepts}{82}{section.133}
\contentsline {subsection}{\numberline {6.1.1}Choice of data}{82}{subsection.134}
\contentsline {subsection}{\numberline {6.1.2}Batch size}{83}{subsection.135}
\contentsline {subsection}{\numberline {6.1.3}Learning rate}{83}{subsection.136}
\contentsline {subsection}{\numberline {6.1.4}Early stopping}{84}{subsection.137}
\contentsline {subsection}{\numberline {6.1.5} Evaluating a classifier }{84}{subsection.138}
\contentsline {section}{\numberline {6.2}CTCF and p63}{85}{section.142}
\contentsline {chapter}{\numberline {7}Architecture}{87}{chapter.144}
\contentsline {section}{\numberline {7.1}The first two layers}{87}{section.145}
\contentsline {section}{\numberline {7.2}Merging the first two layers}{91}{section.154}
\contentsline {section}{\numberline {7.3}The deeper layers}{92}{section.156}
\contentsline {chapter}{\numberline {8}Training results}{97}{chapter.160}
\contentsline {section}{\numberline {8.1}Early stopping}{97}{section.161}
\contentsline {section}{\numberline {8.2}Batch size}{98}{section.163}
\contentsline {section}{\numberline {8.3}Learning rate}{99}{section.165}
\contentsline {chapter}{\numberline {9}Performance and comparison with other tools}{101}{chapter.167}
\contentsline {section}{\numberline {9.1}Cross species}{101}{section.169}
\contentsline {part}{IV\hspace {1em}Discussion}{107}{part.172}
\contentsline {chapter}{\numberline {10}Evolution of Cis regulation}{109}{chapter.173}
\contentsline {section}{\numberline {10.1}Conservation of CREs}{109}{section.174}
\contentsline {subsection}{\numberline {10.1.1}Functional conservation}{110}{subsection.175}
\contentsline {section}{\numberline {10.2}Complexity}{112}{section.177}
\contentsline {subsection}{\numberline {10.2.1}Complexity and WGD}{113}{subsection.178}
\contentsline {section}{\numberline {10.3}Fate}{116}{section.180}
\contentsline {chapter}{\numberline {11}On artificial Neural Networks and TF binding sites}{119}{chapter.181}
\contentsline {chapter}{\numberline {12} Conclusions }{123}{chapter.184}
\contentsline {part}{V\hspace {1em}Methods}{125}{part.191}
\contentsline {chapter}{\numberline {13}Methods}{127}{chapter.192}
\contentsline {section}{\numberline {13.1}PWMs used}{127}{section.193}
\contentsline {section}{\numberline {13.2}TF annotation and TF binding specificity prediction}{128}{section.194}
\contentsline {section}{\numberline {13.3}TF motif mapping onto ATAC-seq peaks}{129}{section.196}
\contentsline {section}{\numberline {13.4}GenomeSizes.ipynb}{129}{section.197}
\contentsline {section}{\numberline {13.5}Intergenic and GREAT size distributions}{133}{section.198}
\contentsline {section}{\numberline {13.6}Make TSS files}{137}{section.199}
\contentsline {section}{\numberline {13.7}Make GREAT-like files}{144}{section.200}
\contentsline {section}{\numberline {13.8}Make Intergenic region files}{148}{section.201}
\contentsline {section}{\numberline {13.9}ChIP-seq overview}{150}{section.202}
\contentsline {section}{\numberline {13.10}ATAC-seq overview}{165}{section.203}
\contentsline {section}{\numberline {13.11}CRE-TSS distances}{172}{section.204}
\contentsline {section}{\numberline {13.12}CRE count distributions}{181}{section.205}
\contentsline {section}{\numberline {13.13}CRE count stratified}{200}{section.206}
\contentsline {section}{\numberline {13.14}Downsampling}{207}{section.207}
\contentsline {section}{\numberline {13.15}Cis-content Phylotypic}{214}{section.208}
\contentsline {section}{\numberline {13.16} Module-module comparisons }{226}{section.209}
\contentsline {section}{\numberline {13.17}NACC}{243}{section.210}
\contentsline {part}{VI\hspace {1em}Appendices}{253}{part.211}
\contentsline {section}{\numberline {13.18}Tables}{255}{section.212}
\contentsline {section}{\numberline {13.19}Nimrod data}{256}{section.213}
\contentsline {section}{\numberline {13.20}ATAC-seq data}{257}{section.216}
\contentsline {section}{\numberline {13.21}Genomes}{257}{section.219}
\contentsline {section}{\numberline {13.22}RNA assays}{257}{section.221}
\contentsline {chapter}{\numberline {14}Scripts}{263}{chapter.226}
\contentsline {section}{\numberline {14.1}ATAC-seq IDR Peak Calling}{263}{section.227}
\contentsline {chapter}{Bibliography}{269}{chapter*.453}
\contentsline {chapter}{List of Figures}{290}{chapter*.456}
\contentsline {chapter}{List of Tables}{291}{chapter*.457}
