\section{ NACC }
\label{chap:nacc}

Looking for evidence of regulatory conservation, we applied Neighborhood Analysis of Conserved Co-expression 
(NACC) \cite{Yue2014}, a method developed to compare heterogeneous, non-matched sample sets across species. This allowed us
to use all of our available RNAseq data without having to worry about matching them across
the species. In total we had 49 Amphioxus, 31 zebrafish, 65 mouse and 52 human samples.
They consist of assays on embryonic stages, organs, cell lines etc. The full list can be
found in \ref{chapter:rnatables}.

The method investigates to what degree gene neighborhoods, genes that are expressed similarly across a set of tissues, remain
as a neighborhood in another species where we can detect the orthologous for each gene. For each gene, we get a NACC score which is smaller
the more conserved the gene's neighborhood is. By plotting the distributions of the NACC values for all of the genes and contrasting it
to the same distribution made with randomized orthologies (Fig. \ref{fig:tfigure_nacc1})
we can see a clear conservation of gene regulation between amphioxus and human.    

Using human as a reference species we applied the NACC analysis to mouse, zebrafish and
amphioxus to reveal an interesting pattern of the 
regulatory similarities as revealed by NACC, increasing with evolutionary proximity (Fig. 
\ref{fig:tfigure_nacc2}).


\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_NACC1} 
\decoRule
\caption[Human-Amphioxus NACC]{ Distributions of NACC values for orthologous genes (in
blue) or random orthology
assignments (red) between Human and Amphioxus. Lower NACC values imply higher
conservation of relative expression.[see notebook:\ref{app:nacc}] }
\label{fig:tfigure_nacc1}
\end{figure}

\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_NACC2} 
\decoRule
\caption[Human-Chordates NACC]{ Distributions of NACC values for orthologous genes (in blue) or random orthology
assignments (red) in three chordate species (mouse, zebrafish and amphioxus) against
human. Lower NACC values imply higher conservation of relative expression. When we randomize the 
orthology connections, the distribution shifts to the right, meaning that on average
genes that are co-expressed in one species, stay co-expressed in the other species as well.
[see notebook:\ref{app:nacc}] }
\label{fig:tfigure_nacc2}
\end{figure}



\section{The phylotypic period} 
\label{chapter:phylotyp}
The phylotypic period is a characteristic shared by
vertebrates when morphological differences are their minimum. That is, vertebrates start their
development with different forms, become more similar to the others at their phylotypic period and
then diverge to the great spectrum of vertebrate form variety.

Previous comparative analyses among vertebrate transcriptomes \cite{Irie2011} also showed a
developmental stage of maximal similarity in gene expression, coinciding with the so-called
vertebrate phylotypic period, in agreement with the hourglass model
\cite{Yanai2018,Duboule1994}. However, similar comparisons with tunicates and amphioxus have
thus far not resolved, at the transcriptomic level, a phylotypic period shared across all chordates \cite{Hu2017}.

As part of our work in \cite{ourpaper}, our collaborators tested whether a period of maximal
gene expression similarity exists between amphioxus and vertebrates.  They did pairwise comparisons
of RNAseq data from developmental time courses in amphioxus, zebrafish, medaka, frog and chicken and
revealed a consistent period of higher similarity between amphioxus and all vertebrate
species (Fig. \ref{fig:tfigure_ferdiphylo}), corresponding to the 4-7 somite neurula (18-21
hpf). 

\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/Inkscape/ferdiphylo.pdf} 
\decoRule
\caption[Phylotypic period: RNA]{ Left) Stages of minimal transcriptomic divergence 
(Jensen-Shannon Distance, JSD) to each
amphioxus stage in four vertebrate species. The grey box outlines the ‘phylotypic’ period of
minimal divergence, with the corresponding vertebrate periods indicated (the range given by
the two closest stages). Dispersions correspond to the standard deviation computed over 100
bootstrap resamplings of the ortholog set. Right) Heatmap of pairwise transcriptomic distances
(Jensen-Shannon metrics) between amphioxus and zebrafish stages. As in (a), smaller
distance (red) indicates higher similarity.}
\label{fig:tfigure_ferdiphylo}
\end{figure}



% \emph{HERE EXPLAIN ABOUT THE PWM CLUSTERS}

Continuing our investigation of cis-regulatory conservation from Chapter \ref{chap:nacc}, we set out to investigate if the
transcriptomic phylotypic dynamics are reflected on the cis-regulatory level.
If gene transcription levels are ultimately controlled by TF-DNA binding events, one would expect
to find similar TF binding sites in the regulatory landscapes of genes that are expressed similarly.    
    
For our analysis (see \ref{app:phyloheat}), we looked into atac peaks that are open in each developmental stage, and counted
statistically significant hits for a large set (see chapter \ref{chap:pwms}) of Position Weight Matrices. Having counts for all the PWMs at each stage,
allows us to compare the stages to each other based on their cis-regulatory content. 

After normalizing the PWM counts, we compute the correlation levels between stages. If two stages are highly correlated it means
that the same PWMs are important or unimportant in those stages.

We present our correlation values in a heatmap, just like in (Fig. \ref{fig:tfigure_ferdiphylo}) (a), to showcase how our analysis revealed
similar dynamics


\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_phyloheat} 
\decoRule
\caption[CIS Phylotypic]{ 
Zebrafish and amphioxus pairwise correlation of
relative TF motif enrichment z-scores in ATAC-seq peaks active at different developmental
stages. Four developmental stages and hepatic tissue from amphioxus are compared against
six developmental stages of zebrafish. [see notebook:\ref{app:phyloheat}] }
\label{fig:tfigure_phyloheat}
\end{figure}


We can also visualize our analysis as a line plot, again to showcase the similarities to the RNA-based comparisons.
Our results are consistent with the hourglass model, with the two most similar stages (in terms of cis-regulatory content) being
those directly preceding the RNA phylotypic period.

\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_phyloline} 
\decoRule
\caption[CIS Phylotypic]{ For each amphioxus
stages (on the x axis), we plot the maximum similarity (the y axis is inverted) to any
zebrafish stage. This similarity is what was plotted in Fig. \ref{fig:tfigure_phyloheat} [see notebook:
\ref{app:phyloheat}] }
\label{fig:tfigure_phyloline}
\end{figure}





\section{ Gene Modules }
\subsection{The WGCNA analysis}
\label{WGCNA}
\ajw{
Having established that at least on some level, there's conservation of gene networks between amphioxus and vertebrates, we
wanted to investigate deeper. As part of our work for \cite{ourpaper}, our collaborators applied a gene clustering analysis
on 17 and 27 RNAseq datasets in amphioxus and zebrafish. The Weighted Gene Correlation Network Analysis clusters together genes 
that are highly correlated, meaning they have similarly low or high transcriptomic levels across the various tissues/samples/organs.

This analysis yielded 25 and 23 clusters in amphioxus and zebrafish, with varying size (number of genes in each cluster). We manually annotated those clusters,
based on their overall gene expression and GO enrichment profiles. As an example, in Fig: \ref{fig:tfigure_module_annotation} 
we show the annotation of two modules in each species. The two on the left were labeled as
'cilium' and the two on the right "Neural tube"
and "Brain" respectively.
}

\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/Inkscape/module_annotation.pdf} 
\decoRule
\caption[WGCNA module annotation]{ 
For two pairs of modules (from left to right, Cilium-amphioxus, Cilium-zebrafish,
Neural Tube-amphioxus, Brain-zebrafish), the distribution of
expression values using the cRPKM (corrected for mappability Reads Per Kbp and Million
mapped reads) metric for all genes within the given module across each sample on the top.
In the bottom, the enriched GO terms within each module (BP, Biological Process; CC,
Cellular Component).
From our work in \cite{ourpaper}}
\label{fig:tfigure_module_annotation}
\end{figure}

\subsection{ Homologous Gene Content }
Having detected modules of genes in both species, and noticing how in some cases their activity is detected in the same
homologous tissues, questions arise, for example; \emph{ are the two muscle modules composed of the same (orthologous) genes? }     
We did pairwise comparisons between the modules of the two species, examining if a given zebrafish module is enriched in 
orthologous genes that are included in any of the amphioxus modules, and present these pairwise enrichments in a heatmap \ref{fig:tfigure_wgcna1}. 
By further clustering the rows and columns of this matrix, we can reveal how some modules of genes have remained tightly 
co-regulated from their common ancestor to amphioxus and zebrafish.



\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_WGCNAHEAT} 
\decoRule
\caption[Module-module homology comparisons]{ Heatmap
showing the level of statistical significance of orthologous gene overlap between WGCNA
modules in the two species as derived from hypergeometric tests. (-log10(pvalue)) [see
notebook: \ref{app:wgcna}] }
\label{fig:tfigure_wgcna1}
\end{figure}


\subsection{ Cis-Regulatory Content }
We continued on the same spirit of comparing the modules between the species, but this time we wanted to
investigate to what degree the modules are similar on their cis-regulatory context. Similarly to what we
did for the phylotypic cis-regulatory analysis, we counted instances of significant PWM hits inside a 
genomic region assigned to each gene. After summing the counts for each module and properly normalizing,
we can compare modules just like we compared developmental stages in Fig. \ref{fig:tfigure_phyloheat}.    
    
We tested the correlation coefficient of these relative motif enrichment scores for all of our
intra-species pairs of modules and found significant positive values for a large fraction of the pairs
that also displayed high homology conservation (in Fig. \ref{fig:tfigure_wgcna1}).
In such cases, the most enriched TF motifs within each cluster were highly consistent between amphioxus
and zebrafish and included TFs with well-known roles in tissue-specific development and
differentiation e.g. Rfx for cilia, Hox for brain, Hnf1a for liver and gut, Ghrl for skin, Mef2 for
muscle, and Elf1 and Spic for immune function. Some of these are shown in Fig. \ref{fig:tfigure_wgcna_from_paper},
and the rest can be computed in the relevant analysis notebook (chapter \ref{app:wgcna}).

In summary, our results show a high level of transcriptomic and cis-regulatory
conservation underlying basic cellular processes and differentiated tissues in adult amphioxus and
vertebrates.


\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_WGCNAHEAT2} 
\decoRule
\caption[Module-module Cis-Regulatory comparisons]{ 
Heatmap of all pairwise
correlations between the modules of the two species, based on the relative TF motif z-scores
for each module. Modules are ordered according to the clustering in Fig. \ref{fig:tfigure_wgcna1}.
 [see notebook: \ref{app:wgcna}] }
\label{fig:tfigure_wgcna2}
\end{figure}



\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/Inkscape/wgcna_from_paper} 
\decoRule
\caption[Module-module comparisons]{ Putting these two heatmaps next to each-other, shows
how some pairs of modules are highly conserved both at the gene and cis-regulatory levels.
The heatmap from Fig. \ref{fig:tfigure_wgcna1} is on the left and the heatmap from Fig. \ref{fig:tfigure_wgcna2}
on the right. Some interesting case are highlighted. From our work in \cite{ourpaper}.
}
\label{fig:tfigure_wgcna_from_paper}
\end{figure}


\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=0.5\textwidth]{../Figures/Inkscape/wgcna_from_paper2} 
\decoRule
\caption[Module-module comparisons]{ Examples of TF binding site motifs with high z-scores from highly correlated 
pairs of modules between zebrafish and amphioxus.From our work in \cite{ourpaper}
}
\label{fig:tfigure_wgcna_from_paper}
\end{figure}
