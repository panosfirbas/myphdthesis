\chapter{Architecture}
\label{ch:nn_arch}


% Following, we present the design of our model and later the comparisons to other tools.


\section{The first two layers}

This model will take as input genomic sequence and ATAC-seq
signal for a window of 1000 basepairs around PWMhits. The
genomic sequence is encoded in the one-hot scheme; for each
genomic position we present five values, one for each
possible base (plus 'n'). Of these five values, at each
position, only the one corresponding to the position's
nucleotide is marked as 1, while the other four are marked
as 0 (Fig. \ref{fig:nimrod0}). 

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]
{../Figures/Inkscape/Nimrod/nn0.png} 
\decoRule
\caption[Nimrod Seq Layer 1 neuron]{ The genomic sequence
signal as a one-hot sequence with zero shown as white and
one shown as black, and a neuron of the first seq-based
layer. The neurons of our model's first layer read seven positions of
the input sequence and output.}
\label{fig:nimrod0}
\end{figure}

The first layer on the genomic sequence consists of neurons
that read 7 basepairs \footnote{ A choice made empirically}
of the raw signal each. In Fig.
\ref{fig:nimrod0} we see one such neuron from layer 1 while in 
\ref{fig:nimrod3} we see all the neurons of layer 2 and the output of
this first layer. Please note that the colors in the matrix positions (squares) in those
figures were an aesthetic choice to show that different positions in those matrices
have different values. The colors do not contain any further meaning.


As was mentioned earlier, each neuron outputs a
number of values,
one for each of the matrices that the layer is designed to
contain. These matrices are 7 columns wide (one for each
position that the neuron 'reads') and 5 rows tall
(one row per possible base, including 'n'), much like a PWM.
In our model, each neuron of the first layer concurrently learns 64 
\footnote{Another empirically made choice, this number should be high enough to
allow capturing complexity but not superfluously high because that slows down
the model unnecessarily} different matrices and
outputs 64 channels of information, or 64 scores of
matrices. The total size of the output of the first layer is
994x64, 994 neurons \footnote{ As many 7bp reading neurons
as you can fit in 1000bp sequence} and 64 channels.


These 64 channels are taken as input by the second layer,
which reads the outputs of 25 of the layer 1 neurons. The
matrices of this layer are 25 columns wide (for each of the
inputs taken) and 64 rows tall, one for each channel that
the underlying layer outputs.

This second layer outputs 128 channels of information (Fig. 
\ref{fig:nimrod3} ).

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{../Figures/Inkscape/Nimrod/nn3.png} 
\decoRule
\caption[Nimrod Seq Layer 2]{ The neurons of Seq Layer 2,
read 25 of the neurons of the underlying layer each. Each neuron then combines the input
values with its inner weight matrices and outputs one value for each for a total of 128
output channels. The inner weight matrices are shared by neurons of the same layer and
are learned during the training phase of the model.    
Please
note that the colors in the matrix positions (squares) in this figure and the following
were an
aesthetic choice to show that different positions in the matrices have different
values. The colors do not contain any further meaning.}
\label{fig:nimrod3}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]
{../Figures/Inkscape/Nimrod/nn3.75.png} 
\decoRule
\caption[Nimrod Seq Layer 1 \& 2]{ AN overview of the first two sequence layers of Nimrod}
\label{fig:nimrod375}
\end{figure}


In Fig. \ref{fig:nimrod375} we see the first two layers of
the model, working on the genomic sequence input. Two more
layers with the same configuration 
\footnote{The neurons of the first layer read 7 positions
and the neurons of the second layer read 25 neurons} are
'built' on top of the ATAC-seq signal (Fig. \ref{fig:nimrod35} ).

The ATACsignal is a base-pair accurate count of cutting
events on the genome. These cutting events, by nature of
paired-end sequencing, have happened in pairs. The distance
of each read to its paired read offers additional
information to the system so we did not want to completely
flatten the signal to a one-dimensional genome-long array.

In that spirit, the ATAC-seq signal is grouped in three
layers; small, medium and large fragment size.


\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]
{../Figures/Inkscape/Nimrod/nn3.5.png} 
\decoRule
\caption[Nimrod ATAC layers]{ The first two layers over the
ATAC-seq signal are almost identical to the ones that we designed over the
genomic sequence. Notice that the input array has 3 channels
instead of the 5 of the sequence-based input array. These layers are almost identical in
architecture but will learn their own inner weight matrices, separately from the sequence
layers.}
\label{fig:nimrod35}
\end{figure}


\section{Merging the first two layers}
\ajw{ 
In the next step, the two outer layers (Seq layer 2 and ATAC
layer 2) get ‘zipped’/merged into a
single layer and the now merged signal gets passed to more layers of the model
for even higher order feature discovery. 
In Fig. \ref{fig:nimrod4} we see how the two layers are
merged into a "zipper" layer. 
}

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]
{../Figures/Inkscape/Nimrod/nn4.png} 
\decoRule
\caption[Nimrod zipper Layer]{ Each neuron of the
zipper layer reads the output of one neuron from Seq
Layer 2 and one from ATAC Layer 2 and outputs 256 channels.
}
\label{fig:nimrod4}
\end{figure}


\section{The deeper layers}
The signal gets condensed
through a max-pooling layer in the next step. In this layer
the neurons do not overlap and so the number of neurons is
drastically reduced from 970 to 194 (Fig. \ref{fig:nimrod5}). The output 256 values each.



\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]
{../Figures/Inkscape/Nimrod/nn5.png} 
\decoRule
\caption[Nimrod Max Pool]{ The neurons of the maxpool layer
don't overlap so their number is reduced dramatically. }
\label{fig:nimrod5}
\end{figure}


\ajw{ 
The signal finally converges to the
output layer of the NN after two more layers, a
convolutional one and a fully connected one which collapses
the network into a single neuron. The output layer at the
end gives us the network's output, two values indicating how
much confidence the network has on applying either of the
potential labels to the input (Fig. \ref{fig:nimrod8}). 

}

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]
{../Figures/Inkscape/Nimrod/nn8.png} 
\decoRule
\caption[Nimrod Max Pool]{ The model as it condenses into a
single pair of values in the layers after the maxpool step. A final convolution layer 
(Combo) reads the output of the maxpool layer and is in turn read by a fully connected
layer which condenses the model's signal in a single neuron that outputs 256 channels.
Those are finally read by the output neuron that outputs 2 values.}
\label{fig:nimrod8}
\end{figure}


% \section{Training the model}

In Fig. \ref{fig:nimrod7} we have a quick overview of our model.

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]
{../Figures/Inkscape/Nimrod/nn7.png} 
\decoRule
\caption[Nimrod Max Pool]{ The model as it condenses into a
single pair of values in the layers after the maxpool step.}
\label{fig:nimrod7}
\end{figure}

Our original input was taken from a 1000bp window around a
single PWM hit on the genome. Sequence and ATAC-seq signal are extracted and given to the
network as input. These arrays of data are fed to the first two layers of the network and
two new arrays are generated, the outputs of Seq layer 1 and ATAC layer 1. The information
continues passing from one layer to the next until the final layer's output where the
network gives us a score for
"this is a true binding site" and a score for "this is a
false site". 

The internal matrices of the network are initialized with random values. At that stage,
the output values of the network are equally random. Our goal is for the network's output
values to be as good of a predictor as possible of the "real" label of any PWM site that
is tested. To do that, we want to change the network's internal matrices, towards values
that make the final output values a good predictor.

The matrices are changed during a training phase. During that phase, for each pwm, we feed
the input genomic sequence and ATAC-seq signal to
the network, and also provide a "real" label, as determined by overlap with an appropriate
ChIP-seq peak. The network uses
these labels as guidance. It compares its output values to the real labels and
changes its internal matrices accordingly.