\section{Under the light of Evolution}

The study of animals and their form is the simplest, most
intuitive first step towards studying life and has kept scholars busy since the times of
Aristotle. Unfortunately comparative anatomy would only be rekindled
in the West after the stupor of ecclesiastical absolutism of the middle ages. We urge the reader to
read E. W. Gudger’s 1934 paper on renaissance and the pioneers of ichthyology in the early 16th
century \cite{Gudger1934}. Among them Pierre Belon who, among other works, published
a comparison of a human to a bird skeleton, and is singled out as the “prophet” of modern
comparative anatomy.
    
By the time Darwin was publishing the origin of species, the concepts of homology and analogy were
well understood by his contemporaries. That was before the general acceptance of phylogeny
\cite{Panchen1999}. As the era of molecular biology dawned, the concept of homology was expanded
to refer to proteins, which were initially investigated based on their chemical similarities
\cite{fitch70} and later on based on their sequence. And just like that, we started talking
about homologous genes.

Molecular phylogenies confirmed some morphological phylogenies, clarified some and
revolutionized others. We discovered that the coding sequences of genes display remarkable
conservation in
the tree of life with some of them, particularly those involved in transcription and translation
being traceable all the way to the universal common ancestor.

On the cis-regulatory level things are less straight forward. While some conservation can
be observed between close species (e.g. human and mouse), and in rare cases over longer
distances (e.g. in all vertebrates) a high degree of turnover is also evident and
cis-regulation conservation seemingly disappears over longer evolutionary distances (e.g.
human-fly)(see Chapter \ref{chapter:discussion_cis_cons}). Overall the conservation of
cis-regulatory elements is poorly understood.

In this work, we investigate the evolution of cis-regulation in a branch of the tree of
life where intriguing questions remain unanswered. In the following sections, we introduce
this branch, our model organism, and what we hope to learn from our analyses.

% When comparing the coding sequences between human and mouse, the
% majority of proteins were found to be identical  \cite{CCDS},yet the two organisms
% display radically divergent morphology. 

% As we discussed earlier, cis-regulation research
% is a newer discipline when compared with other genomic research areas and many mysteries remain
% unsolved.

% Recent work has shown that cis regulatory elements have a higher turnover rate than coding regions.

% Human to mouse comparisons provide us with interesting cases of conserved regulation ++, some of
% them connected to human disease  WHICH? or developmental problems \cite{polydactyly}. However
% while we can detect conservation of coding sequences between human and organisms as evolutionarily
% distant as bacteria (right?), efforts to detect conserved cis-regulation between human and fly or
% caenorhabditis did not yield the expected results. }

% \newpage 
\subsection{ on the Tree of life }

\begin{wrapfigure}{r}{0.5\textwidth} 
% \centering
\includegraphics[width=0.48\textwidth]{../Figures/Inkscape/trees/tree.pdf} 
%\decoRule 
\caption[Vertebrates on the tree of life]{ Some major animal groups in the vicinity of
vertebrates}
\label{fig:Electron} 
\end{wrapfigure}

% We visualize the evolutionary process and the relationships between organisms and groups of
% organisms by constructing phylogenetic trees. In these constructs, broader and more generalised
% groups of organisms are placed in the branches closer to the root and more specific groups are
% placed in the outer. At the edge of the branches, the leaves of the tree, we place our most detailed
% groups, species and subspecies.

The vertebrate pattern is one of life's successful discoveries. Thanks to their high
morphological diversity, they occupy a huge variety of niches. They come in all forms and
shapes and they are of
course of special interest to us since we ourselves belong in this group. Outside of vertebrates,
life can seem quite alien but inside this group our similarities are obvious, vertebrates have a
spine like us, most of them have some form of hands and legs, they have an obvious head and mouth.
They include the vast majority of animals that humans are typically familiar with: fish,
amphibians, reptiles, birds and mammals. Non vertebrates are mostly jelly like, worm-like,
or insects, forms that we cannot empathize with very much.


The most vertebrate-like but non-vertebrate organisms are the cephalochordates \footnote{of which
Amphioxi are the only extant members} and the tunicates. These two groups together with vertebrates
form the group of chordates. Tunicates, also known as urochordates, are marine organisms that
start their lives as tadpole-looking larvae. Most of them later undergo a dramatic metamorphosis into a sack-
like filter-feeding organism that is often attached to rocks or stones.

\begin{figure}[!h] % [!h] if you want the figure put HERE, otherwise automagic 
\centering 
% \includegraphics[width=1\textwidth]{../Figures/Inkscape/methylated}
\includegraphics[width=0.48\textwidth]{../Figures/Inkscape/tunicate2.png}
\includegraphics[width=0.48\textwidth]{../Figures/Inkscape/g14.png} 
\decoRule 
\caption[Tunicate Illustrations]{ The metamoprhosis, on the left, transforms the tadpole to the adult form, on
the right. Illustrations from "A guide to the shell and starfish galleries", Department of Zoology, British museum
(Natural history) 1901.  } 
\label{fig:fig_tunicates} 
\end{figure}

After molecular comparisons became available, it was shown that tunicates are the closest
organisms to vertebrates \cite{delsuc2006tunicates}. This came as a bit of a surprise
since amphioxi look much more like a
‘minimal’ vertebrate than urochordates who look quite alien-like in their adult form. This should
work as a good reminder that small and alien looking life forms aren't less evolved than us. 

\newpage 
\subsection{Amphioxus}
\label{chapter:intro_amphioxus}

\ajw{ Amphioxus is a small, fish-like, benthic organism that lives in shallow waters around the
earth. In fact, about 35 different species have been described, in three genera; Branchiostoma,
Epigonicthys and Asymmetron\cite{kon2007phylogenetic}.

% As was mentioned above, cephalochordates have been established as the most basally divergent
% lineage of chordates and sister group of urochordates and vertebrates.

Amphioxus is a slow-evolving organism and considering how much more it looks like a vertebrate in
comparison to urochordates who are closer to us, it is tempting to consider that this is probably
what the common ancestor chordate looked like.

Amphioxi possess typical chordate characteristics, such as a dorsal hollow neural tube and
notochord, a
ventral gut and a perforated pharynx with gill slits, segmented axial muscles and gonads, a post-
anal tail, a pronephric kidney, and homologues of the thyroid gland and adenohypophysis. However,
they lack typical vertebrate-specific structures, such as paired sensory organs (image-forming eyes
or ears), paired appendages, neural crest cells and placodes(Schubert et al., 2006).
\cite{Bertrand2011}

The many morphological homologies between amphioxus and vertebrates as well as its place
on the
phylogenetic tree are two great motivations to research amphioxus, but its genomic simplicity can be
even more intriguing than its morphological. }

\subsection{Whole Genome Duplications} 
\label{chapter:WGD} 

Among other vertebrate-specific
genomic characteristics such as their large intergenic distances\cite{Lander2001, Nelson2004} or
their high methylation-dependent regulation of embryonic transcriptional enhancers
\cite{Bogdanovic2016}, vertebrates have undergone two rounds of
whole genome duplication (WGD).  Twice at some point near the root of the vertebrate tree 
(details are still being debated) the whole genome was kept in duplicate.
These genome duplication events, have been considered to be major driving forces in the evolution of
vertebrate and plant complexity and form \cite{Holland2008,Semon2007}. 
Interestingly, teleosts, a group of vertebrates with a third duplication, shows further
greater complexity, comprising nearly 50\% of all vertebrate species\cite{delsuc06}
and 99\% of all extant fish species \cite{gsize1}. Despite efforts thought, scientists
have not managed to show this expected increase in morphological diversity in the fossil
record \cite{justnowgd1,justnowgd2}, therefore the link between WGD and increase in animal
shape complexity remains disputed.

Amphioxus doesn't share this genomic characteristic and only has a single, evolved, copy
of the ancestral proto-chordate genome \cite{Dehal2005, Putnam2008}. As a consequence, for
the vast majority of genes that we can detect in amphioxus we can find from a
single up to eight copies of genes in zebrafish and up to four copies in mouse and human. That’s
because teleosts have undergone a third, fish specific genome duplication.
The genes that are kept in multiple copies are vary often related to gene regulation and
development which is how, as we discuss in Chapter \ref{chapter:complexity}, WGDs might
increase the evolutionary potential of organisms.

Once a gene is retained in duplicate, it is interesting to consider what might happen to
the two ohnologues \footnote{Gene duplicates resulting from WGD, in
honor of Susumu Ohno who first cosidered the implications of gene duplication in
evolution \cite{ohno70} }. The \textit{classical}, as of 1999, model of evolution of duplicate
genes predicts that one of the copies will slowly accumulate deleterious mutations until
it degenerates. Force et al. proposed an alternative model, the DDC model and supported
their position with relevant observations \cite{Force1999}. The
Duplication-Degeneration-Complementation (DDC) model, suggests that mutations on the
duplicated
CREs of duplicated genes can increase the probability that the pair of genes will be kept,
and that by sharing deletions of such elements, the copies will partition the
ancestral expression landscape.

The copies, they hypothesize, might each lose function in some of the ancestral domain, a
process called 'subfunctionalization'. Alternatively, one copy would retain all functions
while the other loses more and more expression domains, which is named 'specialization'.
The two might also both keep all ancestral expression ('redundancy') or one or both might
discover new niches of expression ('neofuncionalization').

A further elaboration, the DDI model \cite{ddimodel} (Duplication–Degeneration–Innovation)
argues that
duplicated genes evolve simpler regulatory landscapes during the process of
subfunctionalization, which makes their regulatory landscapes more receptive to change and
thus increases their diversification potential.

Little is know about the actual patterns of gene or cis evolution after WGDs.
Although intuitively attractive, the DDC and DDI hypotheses have been difficult to test
for the vertebrate WGDs due to lack of genome-wide transcriptomic and
regulatory data from appropriate outgroups.  Amphioxus
with its lack of WGDs is a great candidate model organism for these questions so probing
those will be one of our main goals. In the discussion part, we will explore some of the
relevant work and see how our results align with that and the proposed models.



\chapter{ Objectives }

To sum up, the following are some of the objectives that we will try to address in this work:

\begin{enumerate}
	\item To what degree is cis-regulation conserved in chordates? Is the anatomically and morphologically 
	conserved chordate body plan reflected on conserved cis-regulation?
	\item How do duplicated genes evolve after a WGD event? Do our observations
	fit with existing models? What happens to their cis-regulatory landscape?
	\item Histological and morphological complexity has increased in vertebrates. Does
	this reflect on cis-regulation complexity and can we quantify it?
	\item Can a NN that integrates genomic sequence and ATAC-seq signal
	 competently classify TF binding sites ?
	\item Would such a model be able to generalize its TF binding site classification ability in a cross-species manner?

\end{enumerate}


% We will show particular interest on the effect of WGDs on the cis landscapes of genes
% How does WGD enable expanded regulatory potentials? Intuitively, the access to an expanded repertoire of genomic elements is likely to result in higher molecular complexity, and this is particularly the case given the well-established bias for retention of multiple copies of regulatory vs. housekeeping genes [91][98]. But, only recently, with the advent of a wide variety of functional genomics tools, we have started understanding the regulatory paths after WGD. For instance, a recent study comparing transcriptomic and chromatin accessibility data from duplicated (vertebrates) and non-duplicated species (amphioxus) reported marked asymmetric regulatory evolution of members of gene families that have retained multiple copies in vertebrates [19]. In particular, the most common fate is specialization, whereby at least one copy maintains the ancestral breath of expression, and at least another copy gets its expression restricted. 

% In a large fraction of cases, these copies were restricted to a single tissue (often the brain) and underwent high rates of protein coding sequence change, consistent with neofunctionalization or optimization of their function in the specific tissue. Furthermore, genes that have retained multiple copies tend to have larger GRLs with more cis-regulatory elements, although these also occurs asymmetrically among the copies: some have kept similar numbers to the outgroup, but others have undergone dramatic expansions. Strikingly, the specialized copies, and not those expressed broadly, are the ones that have undergone those expansions. In summary, while WGDs may have little organismal impact at the time of the mutation, they pave the way for a major reshaping and expansion of the new genome's regulatory potential.







