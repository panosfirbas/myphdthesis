






# Nimrod

A deep neural network
binary classifier
utilizes sequence and ATACseq signal
separates PWM_sites that do or do not overlap a CHIPSeq peak
detects real binding sites from the full set of binding sites

## Determining Transcription Factor binding sites
# The problem
The accurate identification of TFBS is one of the core challenges of modern genomics.
Although putative regulatory regions can be detected with a variety of approaches, and their function can be confirmed with functional assays, 
the available resolution of the underlying dna-protein interactions remains low.
# The Tools
# CHIPseq: expensive and slow
The most reliable information regarding where YFP is binding on the genome can be obtained with some variation of a chromatine immunoprecipitation assay, which is expensive in material and time while yielding low resulution results (what is the median chip seq width in the encode project?). Efforts are done to improve this (chipmentation).
# Possition weight matrices: innacurate and promiscuous, widely used
For this, a usual complementary proceedure when trying to identify which protein binds where, is to use PWMs. They are promiscuous (what # of motifs from the whole genome overlap chipseq peaks ?), but they allow us to assign binding sites of about 5-25bp. If you know that a region is enriched thanks to a chipseq experiment, and you find a pwm hit in that region, it is fair to be confident that your protein was there.
But what if you dont have a chipsew experiment?
then you have to rely only on sequence but you will need a more aqurate approach than a pwm. 
# Other computational approaches
Most other approaches work by complementing the pwm analysis (cause thats what we do as well) or rely on it for seeding. Unfotunately, no matter hoe accurate, an approach thay relies only on the dna sequence has little value when the investigator is interested in more than one developmental stage, tissue or cell type, since they will always have the same answer.
Examples are the deep sea, +++
Another approach is to use data from a chromatine accessibility experiment like dnase or atac seq. 
# ATACseq and Transcription Factor binding sites
ATAC is cheaper and faster , and in one run gives us information about open chromatine regions, nucleosome positioning, and had been shown that its signal contains information about dna protein interactions
for all factors at the same time.
centipede, wellington, +++
#  Other:
   random forest + nucl.seq-dependent DNAstruct tag:'forest1'
   +++
these approaches usuallu boil down to classifying or scoring pwms:
does it overlap an enhancer region? an atac peak? a footprint?
centipede gives a score to motifs
none of them is a neural network that combines atac seq and sequencing to classify motifs.



## Neural Networks
are the buzz of the day so not much introduction is needed
they have already been used in genomics with great results:
## Our work
Out network consists of a layer over the sequence
a layer over the atac seq signal
and then they merge 
we get a score which can be tweaked for more sensitivity
In more biologist-digestible terms, the first layer on the dna sequence, can be thought of as a PWM of 11bp, computed for positions [0-10], [1-11], [2-12], etc. Each of these positions can be considered as a neuron, who reads 11 input positions,computes a score based on the sequence and the PWM's weights and outputs a value. The weights of this matrix get mutated and optimized during the training faze of the model. 
We multiplex this procedure so that each neuron learns 128 different pwms at the same time. 
The output values (128 values per neuron) will serve as the input of layer 2 which can then be thought of as a PWM_Of_PWMS. If a neuron in layer 1 learns that it is important to have a G at position X, a neuron in layer 2 will learn that is is important for neuron Y of layer 1 to give a high value for matrix#77 (out of 128).
The two order-2 layers are then concatenated, into a fully connected layer,
which is the connected to the output / read out layer.

utilizes sequence and ATACseq signal
is a neural network
binary classifier
can learn from multiple sources
is fucking the f.. its the best thing ever, for reals

## Methods
Describe the model
TensorFlow
The datasets used
The metrics

## Results
This test
That test
Maybe even a third one

## Discussion
We're the best arent we

## Bibliography
https://genome.cshlp.org/content/22/9/1723.full
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5204302/
https://academic.oup.com/bioinformatics/article/32/12/i121/2240609
https://github.com/hussius/deeplearning-biology



# SEQ ONLY
# CNNs
#DeepBind
easiest to run

# CKN-seq
compares to DeepBind
Here, we introduce a hybrid approach between kernel methods and convolutional neural networks for sequences, which retains the ability of neural networks to learn good representations for a learning problem at hand, while defining a well characterized Hilbert space to describe prediction functions. 
https://www.biorxiv.org/content/early/2017/11/10/217257

#TFImpute 
says better than piq, deepbind
trains on stages
https://bitbucket.org/feeldead/tfimpute
# No ATAC, needs either the cell line or the TF to be in the training set
is a deep learning tool based on Theano to predict TF binding. It differs from previous methods by providing a unique imputation ability. It can do cell type specific TF binding prediction even for TF-cell type combinations not in the training set but either TF or cell type is in the training set.

#DNA sequence+shape kernel enables alignment-free modeling of transcription factor binding
No atac
https://bitbucket.org/wenxiu/sequence-shape.git
https://academic.oup.com/bioinformatics/article/33/19/3003/3852080
#DEEP Sea 
the properties and function of DNA sequences
# https://arxiv.org/pdf/1706.00125.pdf
# DANQ
compares to DeepSea
https://github.com/uci-cbcl/DanQ
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4914104/

# ATAC ONLY
# PIQ 
compares to centipede
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3951735/
PIQ is a new computational method that uses machine learning to identify the genomic binding sites of hundreds of transcription factors (TFs) at corresponding motifs from DNase-Seq experiments with accuracy comparable to ChIP-Seq.
 PIQ is a easy-to-use set of 3 scripts which take as input a BAM file, JASPAR formatted motif file and outputs a set of binding site calls and analysis for diagnosing binding accuracy. PIQ depends only on the ability to run R, and either knowledge of running bash scripts or R scripting.
The actively developed branch of PIQ is available from our bitbucket repository here . https://bitbucket.org/thashim/piq-single

# Centipede
http://centipede.uchicago.edu/
CENTIPEDE applies a hierarchical Bayesian mixture model to infer regions of the genome that are bound by particular transcription factors. It starts by identifying a set of candidate binding sites (e.g., sites that match a certain position weight matrix (PWM)), and then aims to classify the sites according to whether each site is bound or not bound by a TF. CENTIPEDE is an unsupervised learning algorithm that discriminates between two different types of motif instances using as much relevant information as possible. 
 DNase-I footprints and PWMs estimated by CENTIPEDE  

#msCentipede
msCentipede is an algorithm for accurately inferring transcription factor binding sites using chromatin accessibility data (Dnase-seq, ATAC-seq) and is written in Python2.x and Cython. The hierarchical multiscale model underlying msCentipede identifies factor-bound genomic sites by using patterns in DNA cleavage resulting from the action of nucleases in open chromatin regions (regions typically bound by transcription factors). msCentipede, a generalization of the CENTIPEDE model, accounts for heterogeneity in the DNA cleavage patterns around sites bound by transcription factors.
seems easier to run
https://rajanil.github.io/msCentipede/

# BinDNASe
compares to Millipede, PIQ, 
BinDNase implements an efficient method for selecting and extracting informative features from DNase I signal for each TF, either at single nucleotide resolution or for larger regions. 
No sequence?
Code not reachable:
http://research.ics.aalto.fi/csb/software/bindnase/.

#ROMULUS
a novel computational method for identifying individual TF binding sites from genome sequence information and cell-type-specific experimental data, such as DNase-seq.
#cmpares to centipede and wellington
R package
https://github.com/ajank/Romulus
https://www.ncbi.nlm.nih.gov/pubmed/27153645
For a given TF, we first identify candidate binding sites that have above-background sequence affinity, using a Position Weight Matrix. Then, following CENTIPEDE, we employ an Expectation–Maximization-based approach to simultaneously learn the DNase I cut profiles and classify the binding sites as bound or unbound by the TF.

# HYBRIDS
# FIDDLE
A generalization of my thing:
https://www.biorxiv.org/content/biorxiv/early/2016/10/17/081380.full.pdf