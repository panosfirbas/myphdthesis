\newpage
\section{ATAC-seq}
\ajw{
Similarly to ChIP-seq, we applied a peak-calling pipeline to
our list of ATAC-seq experiments to obtain ATAC-seq peaks in a
number of developmental stages of amphioxus, zebrafish and
medaka, as well as two cell lines of mouse. [notebook: \ref{app:ATACseqOverview}]


The numbers of ATAC-seq peaks, like the putative enhancers
detected through H3K27ac, are higher in vertebrates than
amphioxus (Fig: \ref{fig:fig_ATACseqOverview}), and
increasing
throughout development (more plots at \ref{app:ATACseqOverview}).
}


\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
% \includegraphics[width=1\textwidth]{../Figures/Inkscape/methylated} 
\includegraphics[width=0.47\textwidth]{../Figures/from_notebooks/tfigure_amphiK4peaks_0.pdf}
\includegraphics[width=0.47\textwidth]
{../Figures/from_notebooks/tfigure_amphiK4peaks_1.pdf}

\includegraphics[width=0.47\textwidth]
{../Figures/from_notebooks/tfigure_amphiK4peaks_2.pdf}
\includegraphics[width=0.47\textwidth]{../Figures/from_notebooks/tfigure_amphiK4peaks_3.pdf}
\decoRule
\caption[ATAC-seq peak counts]{ The number of detected ATAC-seq peaks in our experiments 
[notebook: \ref{app:ATACseqOverview}] }
\label{fig:fig_ATACseqOverview}
\end{figure}


\section{CRE-TSS distances}
\label{chater:cretssdistances}
\ajw{
A more interesting inquiry than the raw number of peaks is how those peaks are distributed on the genome. Since ATAC-seq marks all regulatorily active regions of the genome, we need other clues to inform us on their function.     

Such a clue can be obtained from the genome. A peak that overlaps a TSS is likely the gene’s promoter while a peak at a large distance is likely an enhancer or silencer.

To examine the distribution of peaks around TSSs, we plotted
(Fig: \ref{fig:tfigure_ATACTSS1}) the cumulative distributions of distances from
our various experiments. In this plot, the y axis spans from 0 to 1 and measures the percentage of total peaks that are found at an equal or smaller distance than the value on the X axis.
}

\begin{figure}[!h] % [!h] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_tssDistCDF.pdf} 
\decoRule
\caption[ATAC-seq peak - TSS distances]{ The cumulative distribution function (CDF) of the
distances of ATAC-seq peaks from TSSs. For each point in X, Y percentage of peaks have X
or less distance to a TSS. i.e. roughly 20\% of zebrafish peaks and 80\% of amphioxus are found 
at a distance of 10\^4 or less from a TSS.  [notebook: \ref{app:TSSdistances}] }
\label{fig:tfigure_ATACTSS1}
\end{figure}

\ajw{
We can see a clear difference between amphioxus and the vertebrates, but the vertebrates themselves  are also clearly in order of genome size. Maybe this is simply the effect of larger genomes.
To clarify, we normalized all peak-TSS distances by the average intergenic distance of each genome. In the resulting plot (Fig: \ref{fig:tfigure_ATACTSS1}), the vertebrates behave very similarly to each other while amphioxus is still clearly distinct.

It looks like cis regulatory landscapes of vertebrates have been expanded, with cis regulatory elements being found at larger distances from promoters.
}

\begin{figure}[!h] % [!h] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_TSSdistanceNorm} 
\decoRule
\caption[ATAC-seq peak - TSS distances normalized]{ Like in \ref{fig:tfigure_ATACTSS1} but
now distances to TSS are normalized by the average intergenic distance. i.e. 70\% of
ATAC-seq peaks in vertebrates and more than 80\% in amphioxus are found at a distance of
less than the average intergenic distance. [notebook:\ref{app:TSSdistances}] }
\label{fig:tfigure_ATACTSS2}
\end{figure}



\newpage   

\section{Higher regulatory content }
\label{chapter:morepeaks}
\ajw{
The next question arising through our train of thought is if vertebrate genes are
controlled by more cis regulatory elements than their amphioxus homologues. To investigate
this we need to assign cis elements to genes, which is not a simple task. It is known that cis regulatory elements can be found in surprisingly long distances from their target gene(s), even with other genes found between them. Lacking adequate direct, biologically-collected data with regards to which element interacts with what gene, we necessarily have to adopt a suboptimal scheme.

A typical such approach is to assign each putative element to its nearest gene on the genome. We employed an improvement on this concept, the GREAT method (more in chapter \ref{app:makeGREAT}).

With this, we can count the number of assignable cis
regulatory elements per gene and examine their distribution.
}

\begin{figure}[!h] % [!h] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_dalton2} 
\decoRule
\caption[CRE count distributions 2]{ 
Distribution of the number of ATAC-seq peaks within each gene’s regulatory landscape (as
estimated by GREAT in each of our experiments. For each species (from left to right: amphioxus(ola),
zebrafish (dre), medaka(ola), mouse(mmu)), we plot stage or cell line  [notebook: 
\ref{app:daltons}] }
\label{fig:tfigure_dalton2}
\end{figure}



% TODO ADD HERE DALTONS STRATIFIED
% notebook: \ref{app:daltons_stratified}
% figures
% Figures/from_notebooks/tfigure_stratified1.pdf
% Figures/from_notebooks/tfigure_stratified2.pdf


\subsection{Matched genomic region sizes}

We find more ATAC-seq peaks per gene's GREAT region in vertebrates, but, as we commented in previous sections,  genes in
vertebrates also have larger genomes and larger intergenic regions (Example in Chapter
\ref{chapter:fate1}, Fig. \ref{fig:tfigure_daltonHKTD}). Can the increase in
regulatory content be singularly explained by the increase of space?     
       
We split genes in amphioxus and zebrafish in categories based on the size of their
GREAT region (Fig. \ref{fig:tfigure_strat1} ), by dividing that size by 10000. This way,
category 1 is all genes with a
GREAT region of under 10kb, category 2 is GREAT regions larger than 10kb but smaller than
20kb and so on. We repeated the same process with intergenic regions instead of GREAT
regions in Fig. \ref{fig:tfigure_strat2}.

In both cases, we found that zebrafish genes consistently had more ATAC-seq peaks, even at
comparable genomic sizes, indicating that besides the increase in genomic size, the higher
regulatory content is also  driven by an increase of CRE density.

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_stratified_intergenics.pdf} 
\decoRule
\caption[Matched genomic sizes]{ The counts of ATAC-seq peaks in the intergenic regions of
genes, on the Y axis. Genes are grouped based on the size of their intergenic region. i.e.
category 1 is all genes with a
intergenic region of under 10kb, category 2 is intergenic regions larger than 10kb but smaller than
20kb and so on. }
\label{fig:tfigure_strat2}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_stratified1.pdf} 
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_stratified2.pdf} 
\decoRule
\caption[Matched genomic sizes]{ The counts of ATAC-seq peaks in the GREAT regions of
genes, on the Y axis. Genes are grouped based on the size of their GREAT region. i.e.
category 1 is all genes with a
GREAT region of under 10kb, category 2 is GREAT regions larger than 10kb but smaller than
20kb and so on.  }
\label{fig:tfigure_strat1}
\end{figure}



\newpage
\subsection{Downsampling}

To examine the possibility that this increase is a result of richer, more deeply sequenced experiments, we down-sampled the zebrafish experiment. 
That is, we randomly discard a portion of the reads of a rich experiment and repeat the
peak-calling procedure. We removed more and more of reads from a zebrafish and a medaka
experiment and found that we had to lower the coverage of vertebrates to 20\% of our
richest amphioxus coverage in order to break the effect.

\begin{figure}[!h] % [!h] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_downsampling} 
\decoRule
\caption[Downsampling]{ The counts of CREs
at increasing levels of downsampling [see notebook: \ref{app:downsampling}]. At 100, is
the mean coverage (sequencing reads per effective genome size) of our two richest
amphioxus replicates. At 101 is the peaks found in the slightly richer of those two
replicates alone. In zebrafish and medaka, we tried increasingly harsh downsampling in
order to bring the coverage of those experiments at lower levels than the ones from
amphioxus. }
\label{fig:tfigure_downsampling}
\end{figure}
