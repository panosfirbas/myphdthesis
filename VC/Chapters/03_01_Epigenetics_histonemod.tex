
\section{Histone modifications}
\ajw{
The chromatin modifiers are enzymes that apply a range of post-translational modifications
of the most abundant DNA-binding proteins; the histones. The DNA molecule is packed
tightly in the nuclei of eukaryotes, wrapped around octamers of the “core” histones (one copy of H3-H4 tetramer and two copies of H2A-H2B dimer) whose amino-terminal tails pass over and between the DNA superhelix to contact neighboring particles\cite{Luger97}.

Most nucleosomes also recruit either histone H1 or high mobility group (HMG) proteins (sometimes both) which bind to the outside of the nucleosome to form a particle known as the chromatosome\cite{COCKERILL}.

The first connection of histone modifications to gene regulation was proposed surprisingly early in 1964 when, with a working model that considered histones as proteins that bind to DNA and repress RNA transcription, Allfrey and Mirsky\cite{ALLFREY64} presented their findings that acetylation and methylation of histone takes place post-translationally and that acetylation specially “may affect the capacity of the histones to inhibit ribonucleic acid synthesis in vivo”\cite{ALLFREY64}.

Since then, the nucleosome was described and a lot of the details of the above model were illuminated. The N-terminal tails of histones, where most modifications take place, can undergo a large variety of modifications (at least eight distinct types) but acetylation, methylation and phosphorylation have dominated scientific interest \cite{kouz07}. Besides modifications of the N tails, reports of modifications located within the globular domain of histones are surfacing \cite{Kebede2015}.    

In the ‘beads on a string’ model, the DNA molecule is first wrapped around histones and then wrapped into higher order structures facilitated by the histones. The nucleosomes can now be thought of as a sequence of units which, besides the raw genomic sequence, also vary in nucleosomal structure, thanks to modifications. The beaded string now forms a new ‘primary structure’ of chromatin which can give rise to a multitude of higher order structures\cite{Luger12}.

The tight packaging essentially blocks access to the DNA molecule, which is a double edged sword. On one hand, foreign DNA such as retroviral elements are silenced. On the other hand, of course, the cell needs access to the DNA for many processes. To overcome this, cells employ specialized chromatin remodeling complexes which either reorganize nucleosomes directly in an ATP dependent manner, or toggle modifications on the histone tails which can alter nucleosome structure, stability and dynamics. \cite{zheng13}

To study these modifications in the modern context, we typically perform ChIP-seq assays with antibodies that recognize the modified histone. This isolates the fragments of DNA that were in proximity to the target histone. We then sequence the fragments and map them on a reference genome to acquire genomic tracks of said modification. Regions of the genome that harbored our target modified histone in a significant percentage of the cell population of the sample will give a stronger signal in the sequencing experiment.
}


\begin{figure}[!h] % [!h] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/Inkscape/chromatinstates.pdf} 
\decoRule
\caption[Histone modifications simple model]{ Histone modifications influence the way that
nucleosomes interact with other nucleosomes and consequently influence the local
chromatin state. A modification that makes the nucleosomes pack together tightly (here, in the bottom row) will
solidify the chromatin and repress local gene expression since it doesn't allow DNA to be
accessed by proteins. In contrast, an enhancing modification (top row), would relax the packing of
the chromatin, facilitating the binding of transcriptionally relevant proteins on the DNA.
}
\label{fig:fig_chromatinstates}
\end{figure}

% #https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3941222/
% #http://science.sciencemag.org/content/356/6335/320?casa_token=VweihhvA_TsAAAAA%3A7aJdHMvhDszaGKXZg6STcEYLeQDeeOKPUabD_IzH43Qy_YjjnktI5_ibe9jGQKr18u4CZG3A-wM
% #HIstone modifications
% #1966: RNA synthesis and histone acetylation during the course of gene activation in lymphocytes. 
% # https://www.ncbi.nlm.nih.gov/pmc/articles/PMC224233/ 
% # {COCKERILL}. Has an amazing figure on the nucleosome



\subsection{H3K4me3}
\ajw{
The tri-methylation of the fourth lysine of histone 3, one of the post translational
modifications of histones that were just discussed, is ubiquitously found on the promoters
of eukaryotic genes that are undergoing transcription \cite{cruz2018tri}. It is
mediated by the Set1 protein and the COMPAS (Complex Proteins Associated with Set1)complex
\cite{compass} which are recruited in the promoter by the RNA polymerase II elongation
factors Paf and FACT\cite{cruz2018tri}.

% was shown to be mediated by the Set1
% protein in yeast and in contrast to the di-methylated state, which is mediated by the same
% protein, was found exclusively at the 5' of genes undergoing active transcription\cite{santosrosa02}.


H3K4me3 activity is conserved in eukaryotes but not without differences. Specifically
in chicken, it was observed that while H3K4me3 can be used to classify genes in active and inactive and preferentially associates with the transcribed regions of genes, it can still be detected in lower quantities on inactive genes\cite{Schneider03}.

In humans, H3K4me3 is mediated by Set9, which competes with histone deacetylases and precludes the H3k9 methylation by Suv39h1\cite{nishi02}. Up to 75\% of our genes were found to be tri-methylated even without detectable RNA elongation \cite{guenther07}.

Nevertheless, H3K4me3 was one of the epigenetic marks most employed by the ENCODE project in its attempt to identify functional elements on the human genome. At the time of writing, 168 different ChIP-seq assays against H3K4me3 are available for human cells and tissues on the ENCODE project’s page and 106 for mouse. These assays were conducted in order to characterize promoter regions. It is believed that nucleosomes that are modified by H3K4me3 form a chromatin state that somehow facilitates transcription initiation.

In zebrafish too, H3K4me3 has been shown to be enriched at transcriptional
start sites, an association which correlates with gene
expression \cite{Aday2011,Bogdanovic2012}. 

We contribute to the effort to investigate this modification by providing H3K4me3 assays in two new developmental stages of zebrafish, significantly enhancing the resolution of the available data during the development of the popular model organism.
}

\subsection{H3K27ac}
As we discussed earlier, TFs bind on DNA in sites both proximal and distal to TSSs. The regions of these distal binding sites usually behave as ‘hotspots’, offering binding sites for multiple TFs. We can think of those regions as functional units of the gene regulation system and we can categorize them depending on further characteristics such as chromatin state, or chromatin accessibility (see later chapter). 

H3K27ac is another heavily investigated histone modification (ENCODE: 97 in human, 93 in mouse). Acetylation has long been associated with transcriptional activation, in contrast to histone deacetylation which is generally thought to have roles in transcriptional repression\cite{zheng13}.

Originally investigated in yeast  \cite{o_k27ac_yeast} and later in human
and mouse \cite{o_k27_human}, H3K27ac was initially reported to be highly enriched at
promoter regions of transcriptionally active genes \cite{wang2008} but later was
also established as an important mark to detect active enhancers\cite{creyghton2010,
radaiglesias11, bonn12}, including distal cis regions which seemingly increase gene
transcription levels. H3K27ac has also been investigated in early zebrafish development
where it was shown to positively correlate with gene expression \cite{Bogdanovic2012}.

In this work, we present H3K27ac assays in two new developmental stages of zebrafish, significantly enhancing the resolution of the available data during the development of the popular model organism.

% ##???
% This feels a bit weak, i want to say more about distal elements but i also want to save it for later chapters
% Other papers?
% https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3003124/
