\subsection{ Gene Fate after WGD }
\ajw{
As was mentioned before, Whole Genome Duplications are
significant events in the developmental history of organisms.
Comparing the slow evolving amphioxus's genome which contains
no duplications to vertebrate genomes that contain at least
two, allows us to investigate what happens to genes after
a WGD event.

Based on the reconstructed homologous gene families, we split genes
in categories based on how many copies of the gene have been retained in
mouse. This yielded four categories of genes. The first one, where the 
gene has been retained in a single copy in mouse ('1-1'), is enriched for genes that 
can be labeled as "House Keeping" (see \ref{app:daltons}). The other three 
categories, where genes are retained from 2 to 4 copies, are in contrast
enriched for genes implicated in transcriptional regulation
or development.

By splitting the genes in these categories we can elucidate how
Trans-Dev genes are more likely to be retained in multiple copies,
as was discussed in \ref{chapter:WGD}, and how these genes are more
likely to have larger regulatory landscapes with more cis-regulatory elements
both in amphioxus and in vertebrates (Fig. \ref{fig:tfigure_dalton_GR} and \ref{fig:tfigure_dalton3})
The increase in zebrafish was not attributable to the third WGD of teleosts, because using 
published ATAC-seq datasets we found an even stronger pattern for mouse (see \ref{app:daltons}).

Plotting only the 'Housekeeping' and 'Trans-Dev' labeled genes also highlights the same dynamic (Fig. \ref{fig:tfigure_daltonHKTD})
}

\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=0.5\textwidth]{../Figures/from_notebooks/tfigure_daltonGR.pdf}
\decoRule
\caption[GREAT sizes homology groups]{ The size of gene GREAT regions,
split in categories based on WGD retention [see notebook: \ref{app:daltons}] }
\label{fig:tfigure_dalton_GR}
\end{figure}


\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=0.5\textwidth]{../Figures/from_notebooks/tfigure_dalton3.pdf}
\decoRule
\caption[CRE counts homology groups]{ The distribution of counts of CREs in gene GREAT regions,
split in categories based on WGD retention [see notebook: \ref{app:daltons}] }
\label{fig:tfigure_dalton3}
\end{figure}

\subsection{ Daltons per TD-HK }
\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_TDHK.pdf}
\decoRule
\caption[CRE counts HousekeepingTransdev]{The distribution of counts of CREs in gene GREAT regions,
split in Transdev and Housekeeping subsets [see notebook: \ref{app:daltons}] }
\label{fig:tfigure_daltonHKTD}
\end{figure}

\subsection{ Daltons per 1-X max }
\ajw{
	Interestingly, the number of CREs is very uneven between ohnologs \footnote{Gene copies resulting from WGDs}:
   the paralog with the lowest number of associated CREs generally has a comparable number to the amphioxus ortholog, 
   but dramatic regulatory expansions were observed for some ohnologs (Fig. \ref{fig:tfigure_dalton4}). 
   The same patterns were detected for all amphioxus and zebrafish developmental stages (see notebook: \ref{app:daltons}).
}
\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_dalton4.pdf}
\decoRule
\caption[CRE counts homology groups MINXMAN]{The distribution of counts of CREs in gene GREAT regions,
split in categories based on WGD retention. Only the minimum and maximum count per gene family is shown  [see notebook: \ref{app:daltons}] }
\label{fig:tfigure_dalton4}
\end{figure}


\subsection{ Increased regulatory complexity in functionally specialized ohnologs }
% \begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
% \centering
% \includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_fates.pdf}
% \decoRule
% \caption[CRE counts by WGD fate]{The distribution of counts of CREs in gene GREAT regions,
% split in categories based on WGD fate [see notebook: \ref{app:daltons}] }
% \label{fig:tfigure_daltonfate}
% \end{figure}

% \begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
% \centering
% \includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_domlost.pdf}
% \decoRule
% \caption[CRE counts by domains lost]{The distribution of counts of CREs in gene GREAT regions,
% by number of expression domains lost when compared to amphioxus homologue [see notebook: \ref{app:daltons}] }
% \label{fig:tfigure_daltonDomLost}
% \end{figure}



\ajw{
Examining the fate of a gene's regulation after a WGD, the Duplication-Degeneration-Complementation (DDC) model 
hypothesizes that duplicated genes can split the ancestral expression domains by reciprocally losing CREs which in
theory allow a gene to be expressed in 'X' context (Fig \ref{fig:tfigure_genefate2} (ii) sufunctionalization). 

\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/Inkscape/g11264.png}
\decoRule
\caption[Gene Fate ]{ from our work in \cite{ourpaper}}
\label{fig:tfigure_genefate2}
\end{figure}
}
\ajw{
The non duplicated genome of amphioxus is a unique opportunity 
to study this.

Although intuitively attractive, this
hypothesis has been difficult to test for the vertebrate WGDs due to lack of transcriptomic and
regulatory data from appropriate outgroups. 

DDC predicts that individual duplicate genes would each have more restricted expression than an unduplicated
outgroup, but their summation would not. To
investigate this, our collaborators focused on seven homologous tissues and two equivalent developmental stages in
amphioxus, zebrafish, frog and mouse, and marked the expression of each gene in each sample as on or
% off based on fixed cut-offs. By counting an 'expression bias' metric \[ \delta_zebra = Num. domains expressed in zebrafish - Num. domains expressed in amphioxus \]
for each gene and plotting the distribution of these values, they 
detected no difference between amphioxus and vertebrates
in expression bias (Fig. \ref{fig:tfigure_genefate1} (a) ). 

\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/Inkscape/g10943.png}
\decoRule
\caption[Gene Fate ]{ from our work in \cite{ourpaper}}
\label{fig:tfigure_genefate1}
\end{figure}
}
\ajw{
In contrast, when
vertebrate genes from families with multiple ohnologs were compared to their single amphioxus
ortholog, the distributions were strongly skewed, with many vertebrate genes displaying far more
restricted expression domains  (Fig. \ref{fig:tfigure_genefate1} (a). 

Remarkably, the symmetrical
pattern was fully recovered when the expression of all vertebrate members was combined or the raw
expression values summed for each member within a paralogy group ( Fig. \ref{fig:tfigure_genefate1} (a), \cite{ourpaper} ). 

When multiple genes are retained after WGD, many gene copies restrict their expression
domains, but the joint expression of all members is similar to that of the single amphioxus
ortholog, and likely of the ancestral gene.

Although the above findings are consistent with the DDC model, they are also compatible with an
alternative model in which a subset of duplicate genes becomes more ‘specialized’ in expression
pattern while one or more paralogs retain the ancestral broader expression. To distinguish between
these alternatives, we analyzed a subset of multi-gene families in which both the single amphioxus
ortholog and the union of the vertebrate ohnologs were expressed across all nine compared samples. 

We then identified (Fig. \ref{fig:tfigure_genefate1} c): (i) gene families in which all vertebrate
paralogs were expressed in all domains (‘redundancy’), (ii) gene families in which none of the
vertebrate members had expression across all domains (‘subfunctionalization’), and (iii) gene
families in which one or more vertebrate ohnologs were expressed in all domains, but at least one
ohnolog was not (‘specialization’). 

We obtained very similar results for the three studied
vertebrate species (\ref{fig:tfigure_genefate3} a): between 80 and 88\% of gene families fell into categories (ii) or
(iii), with loss of ancestral expression domains in at least one member. Moreover, we found
specialization (iii) to be consistently more frequent than subfunctionalization (ii) as a fate for
vertebrate ohnologs.

\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/Inkscape/g11591.png}
\decoRule
\caption[Gene Fate ]{ from our work in \cite{ourpaper}}
\label{fig:tfigure_genefate3}
\end{figure}
}
\ajw{
Interestingly, ohnologs that have experienced strong specialization (defined as having two or fewer
remaining expression domains) showed the
fastest rates of sequence evolution and the highest dN/dS ratio between human and mouse, whereas
genes from redundant families and those ohnologs from specialized families that retain ancestral
expression displayed the lowest levels of sequence divergence (\ref{fig:tfigure_genefate3} b). 

Strikingly, we found that ohnologs from specialized families that have lost expression
domains showed significantly more associated regulatory elements than those with the full ancestral
expression (\ref{fig:tfigure_genefate3} c). In fact, we observed a strong positive relationship between the number of
ancestral expression domains lost and the number of putative regulatory elements associated with
specialized ohnologs (
% Fig. \ref{fig:tfigure_domlost}
). 

This implies that specialization of gene expression
after WGD does not occur primarily through loss of ancestral tissue-specific regulatory elements,
but rather by complex remodeling of regulatory landscapes involving recruitment of novel tissue-
specific regulatory elements.

}
