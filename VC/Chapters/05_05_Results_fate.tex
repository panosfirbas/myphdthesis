\chapter{ Regulatory content and gene fate after WGD }
\subsection{ Gene Fate after WGD }
\label{chapter:fate1}
\ajw{
As was mentioned before, Whole Genome Duplications are
significant events in the developmental history of organisms.
Comparing the slow evolving amphioxus's genome which contains
no whole-genome duplications to vertebrate genomes that contain at least
two, allows us to investigate what happens to genes after
a WGD event.

Based on the reconstructed homologous gene families, we split genes
in categories based on how many copies of the gene have been retained in
mouse, to keep comparisons between vertebrates consistent. 
This yielded four categories of genes. The first one, where the 
gene has been retained in a single copy in mouse ('1-1'), is enriched for genes that 
can be labeled as "House Keeping" (see \ref{app:daltons}), that is genes that are involved in basic cellular functions that are shared acroos cell types. 
The other three 
categories, where genes are retained from 2 to 4 copies, are in contrast
enriched for genes implicated in transcriptional regulation
or development.

By splitting the genes in these categories we can elucidate how
Trans-Dev genes are more likely to be retained in multiple copies,
as was discussed in \ref{chapter:WGD}, and how these genes are more
likely to have larger regulatory landscapes with more cis-regulatory elements
both in amphioxus and in vertebrates (Fig. \ref{fig:tfigure_dalton_GR})
The increase in zebrafish was not attributable to the third WGD of teleosts, because using 
published ATAC-seq datasets we found an even stronger pattern for mouse (see the notebook in chapter \ref{app:daltons}).

Plotting only the 'Housekeeping' and 'Trans-Dev' labeled genes also highlights the same dynamic (Fig. \ref{fig:tfigure_daltonHKTD})
}

\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=0.48\textwidth]{../Figures/from_notebooks/tfigure_daltonGR.pdf}
\includegraphics[width=0.48\textwidth]{../Figures/from_notebooks/tfigure_dalton3.pdf}
\decoRule
\caption[Homology groups - Landscape sizes and CRE counts]{ The size of gene GREAT regions
(left) and counts of
CREs in gene GREAT regions (right) split in categories based on WGD retention. Genes found
in a single copy in both amphioxus and mouse are denoted as '1-1'. Genes found in two
copies in mouse but in a single copy in amphioxus are '1-2' and so on.[see notebook: 
\ref{app:daltons}] }
\label{fig:tfigure_dalton_GR}
\end{figure}

\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_TDHK.pdf}
\decoRule
\caption[CRE counts HousekeepingTransdev]{The distribution of counts of CREs in gene GREAT regions,
split in Transdev and Housekeeping subsets [see notebook: \ref{app:daltons}] }
\label{fig:tfigure_daltonHKTD}
\end{figure}

% \begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
% \centering
% \decoRule
% \caption[CRE counts homology groups]{ The distribution of counts of CREs in gene GREAT regions,
% split in categories based on WGD retention [see notebook: \ref{app:daltons}] }
% \label{fig:tfigure_dalton3}
% \end{figure}

% \subsection{ Daltons per TD-HK }
% % #######################################################################################
% \label{chapter:fate2}

\subsection{ Daltons per 1-X max }
% #######################################################################################
\label{chapter:fate3}

Interestingly, the number of CREs is very uneven between ohnologs \footnote{Gene duplicates resulting from WGD, in
honor of Susumu Ohno who first considered the implications of gene duplication in
evolution \cite{ohno70} }:
the paralog with the lowest number of associated CREs generally has a comparable number to the amphioxus ortholog, 
but dramatic regulatory expansions were observed for some ohnologs (Fig. \ref{fig:tfigure_dalton4}). 
The same patterns were detected for all amphioxus and zebrafish developmental stages (see notebook: \ref{app:daltons}).

\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_dalton4.pdf}
\decoRule
\caption[CRE counts homology groups MINXMAN]{The distribution of counts of CREs in gene GREAT regions,
split in categories based on WGD retention. Only the minimum and maximum count per gene family is shown  [see notebook: \ref{app:daltons}] }
\label{fig:tfigure_dalton4}
\end{figure}


\subsection{ Increased regulatory complexity in functionally specialized ohnologs }
\label{chapter:fate4}
% #######################################################################################

% Examining the fate of a gene's regulation after a WGD, the Duplication-Degeneration-Complementation (DDC) model 
% hypothesizes that duplicated genes can split the ancestral expression domains by reciprocally losing CREs which in
% theory allow a gene to be expressed in 'X' context (Fig \ref{fig:tfigure_genefate2} (ii) sufunctionalization). 



DDC predicts that individual duplicate genes would each have more restricted expression than an unduplicated
outgroup, but their summation would not. To
investigate this, our collaborators focused on seven homologous tissues and two equivalent developmental stages in
amphioxus, zebrafish, frog and mouse, and marked the expression of each gene in each sample as on or
off based on fixed cut-offs. 

By counting an expression-bias metric that they called \textit{delta zebra},
for each gene and plotting the distribution of these values, they investigate differences between amphioxus and vertebrates (Fig. 
\ref{fig:tfigure_genefate1}). The metric is defined as the number of domains expressed in zebrafish minus the number of domains
expressed in amphioxus. The left skewing of the distribution plotted in the middle of  Fig.\ref{fig:tfigure_genefate1} for example,
shows how the majority of ohnologs in zebrafish lose expression domains in comparison to their amphioxus counterparts. 


\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/Inkscape/g10943.png}
\decoRule
\caption[Gene Fate ]{ 
Distribution of the difference in positive domains between zebrafish and
amphioxus for 1-to-1 orthologs (left), individual ohnologs (middle) and the union of all
vertebrate ohnologs in a family (right).  See Fig. \ref{fig:tfigure_genefate2} for
schematic of how the values are calculated.
}
\label{fig:tfigure_genefate1}
\end{figure}

For genes that are conserved in single copies in vertebrates (Fig. 
\ref{fig:tfigure_genefate1} left), the distribution of values
is centered around 0, indicating that this group of genes tends to retain the ancestral
expression domains. In contrast, when
vertebrate genes from families with multiple copies were compared to their single
amphioxus ortholog, the distributions were strongly skewed, with many vertebrate genes
displaying far more restricted expression domains  (Fig. 
\ref{fig:tfigure_genefate1} middle).
Remarkably, the symmetrical
pattern was fully recovered when the expression of all vertebrate members was combined or the raw
expression values summed for each member within a paralogy group ( Fig. 
\ref{fig:tfigure_genefate1} right). 


Although the above findings are consistent with the DDC model, they are also compatible with an
alternative model in which a subset of duplicate genes becomes more ‘specialized’ in expression
pattern while one or more paralogs retain the ancestral broader expression. To distinguish between
these alternatives, we analyzed a subset of multi-gene families in which both the single amphioxus
ortholog and the union of the vertebrate ohnologs were expressed across all nine compared samples. 

We then identified (Fig. \ref{fig:tfigure_genefate2} c): (i) gene families in which all
vertebrate
paralogs were expressed in all domains (‘redundancy’), (ii) gene families in which none of the
vertebrate members had expression across all domains (‘subfunctionalization’), and (iii) gene
families in which one or more vertebrate ohnologs were expressed in all domains, but at least one
ohnolog was not (‘specialization’). 
% this is the dots model of DDC
\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/Inkscape/g11264.png}
\decoRule
\caption[Gene Fate - Schematics ]{ Left: Schematic summary of the analysis shown in Fig. 
\ref{fig:tfigure_genefate1}.
Expression is binarized (on or off) for
each amphioxus and vertebrate gene across nine comparable samples, based on an
arbitrary expression cut-off (normalized cRPKM>5). For each vertebrate gene, the number of
positive expression domains is subtracted from the number of domains in which the single
amphioxus ortholog is expressed. Black/White circles represent on/off expression,
respectively.
Right: Schematic summary
of the analyses shown in Fig. \ref{fig:tfigure_genefate3}, 
representing the three possible fates after WGD: Redundancy,
all ohnologs are expressed in all domains; Subfunctionalization, none of the ohnologs are
expressed in all domains; Specialization, at least one of the ohnologs in expressed in all
domains, but at least one is not. 
}
\label{fig:tfigure_genefate2}
\end{figure}

We obtained very similar results for the three studied
vertebrate species (\ref{fig:tfigure_genefate3} a): between 80 and 88\% of gene families fell into either
subfunctionalization or specialization, meaning they show a loss of ancestral expression domains in at least one member. 
Moreover, we found specialization to be consistently more frequent than subfunctionalization as a fate for
vertebrate ohnologs.

\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/Inkscape/g11591.png}
\decoRule
\caption[Gene Fate - Distributions]{ Left: Distribution of fates after WGD for families of
ohnologs
inferred to be ancestrally expressed in all nine studied domains for each vertebrate species. 
Right: Distribution of the percentage of nucleotide
sequence similarity between human and mouse for different classes of ohnologs based on
their fate after WGD. Ohnologs from specialized families are divided into “Spec. equal”
(maintaining all expression domains), “Spec. mild” (which have lost expression domains, but
maintained more than two), “Spec. strong” (with two or fewer remaining expression
domains) }
\label{fig:tfigure_genefate3}
\end{figure}

Interestingly, ohnologs that have experienced strong specialization (defined as having two or fewer
remaining expression domains) showed the
fastest rates of sequence evolution and the highest dN/dS ratio between human and mouse, whereas
genes from redundant families and those ohnologs from specialized families that retain ancestral
expression displayed the lowest levels of sequence divergence (\ref{fig:tfigure_genefate3} b). 

Surprisingly, specialization and subfunctionalization were not correlated with an obsious
loss of CREs as the DDC model would assume. In fact, we observed that these categories of
genes were more likely to have more CREs found inside their cis landscapes (Fig. \ref{fig:tfigure_daltonfate})
\label{chapter:fate4}
\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_fates.pdf}
\decoRule
\caption[CRE counts by WGD fate]{Here we plot the distribution of counts of CREs in gene
GREAT regions. We split the genes in categories based on WGD fate, as was discussed in the text and 
shown in the previous figures [see notebook: \ref{app:daltons}] }
\label{fig:tfigure_daltonfate}
\end{figure}

Furthermore, we found that ohnologs from specialized families that have lost expression
domains showed significantly more associated regulatory elements than those with the full ancestral
expression (\ref{fig:tfigure_genefate3} c). In fact, we observed a strong positive relationship between the number of
ancestral expression domains lost and the number of putative regulatory elements associated with
specialized ohnologs (Fig. \ref{fig:tfigure_domlost})

\begin{figure}[H] % [H] if you want the figure put HERE, otherwise automagic
\centering
\includegraphics[width=1\textwidth]{../Figures/from_notebooks/tfigure_domlost.pdf}
\decoRule
\caption[CRE counts by domains lost]{The distribution of counts of CREs in gene GREAT regions,
by number of expression domains lost when compared to amphioxus homologue [see notebook: \ref{app:daltons}] }
\label{fig:tfigure_domlost}
\end{figure}


This implies that specialization of gene expression
after WGD does not occur primarily through loss of ancestral tissue-specific regulatory elements,
but rather by complex remodeling of regulatory landscapes involving recruitment of novel tissue-
specific regulatory elements.


