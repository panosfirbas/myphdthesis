**\
Regulation**

**Transcription regulation**

** **

**Trans-Regulation**

** DNA Binding Proteins**

** Binding sites**

** Wet lab techniques**

** TF CHIPseq**

** Dry lab techniques**

** PWMs**

** Other feature-based approaches**

** Wellington**

** centipede**

** PIQ?**

** &lt;see notes on nimrod&gt;**

** Other seq based approaches**

** ++**

** Neural Networks**

** DeepBind**

** DeepSea**

** TFImpute**

** Our Hybrid Approach**

** &lt; describe nimrod as a NN &gt;**

**Epigenetics**

** Methylation**

** Histone modifications**

** H3k4me3**

** H3k27ac**

** Chromatin accessibility**

** A bit on TADs**

**Cis-Regulation**

** A bit on promoters**

** A bit on enhancers**

** **

**Cis Regulation and Evolution\
\
\
**

<span id="anchor"></span>Regulation

All cells in an organism share the same genetic library. The dynamics
that drive the differential usage of this library, in time and in space,
allowed multicellularity, tissue specification, organ-formation, axis
formation etc. In other words, without the ability to use the genetic
library in various ways, the clones of cells would only be able to form
amorphous blobs instead of the great variety of organisms that exist.

These dynamics, or regulation of gene expression (gene-regulation), are
controlled by mechanisms in essentially every step of the production of
a gene product (protein or RNA). They dictate how much, of which
protein, at what time, will be produced.

We will focus on the first and most important of those steps, the
regulation of transcription; how much RNA will be produced from a given
gene. Later steps such as rna-processing, rna-transfer or translation
are also important, but out of the scope of this work.

During development, an organism has to unpack all of its complexity out
of a single cell, from a single copy of its DNA material (and some
maternal RNA that jump-starts the process). It is in this stage of an
organism’s life that we encounter the greatest complexity in gene
expression patterns, and where gene-regulation mechanisms have the
biggest effects.

Although individuals with identical genotype display, predictably,
higher similarity of gene expression than related or non-related
individuals \\cite{Cheung2003}, we can still detect differences which
seem to increase with age \\cite{Fraga2005}. These subtle regulatory
differences in homozygotic tweens for example could lead to the subtle
phenotypic differences between those individuals.

The slightly larger, compared to twins, differences in genetic makeup
and gene-expression that one would expect to find in a population give
us the differences in phenotype between non-related individuals, while
even larger differences give us the differences between species.

Since changes on gene regulation have such a strong impact on organism
morphology,

they apply strong evolutionary pressure

Are under evolutionary selection ++

<span id="anchor-1"></span>Transcription regulation

Twenty three years ago, when only few sequences larger than 20kb could
be compared across mammals,

Ben F. Koop talks about a non coding 120bp region in the 5’ of Ca gene,
which is “conserved in mouse but also found in fish, birds, and
Xenopus”, and was shown to “bind proteins and alter the levels of RNA
transcription”, but he does not have the vocabulary to call the region
by any other name. \\cite{Koop1995}

Since then, thanks to advances in our understanding of genomics and
epigenomics, in Next Generation Sequencing (NGS), etc. we can talk about
enhancers, promoters, insulators, ultra-conserved regions, transcription
factors, transcription-factor binding sites, chromatin accessibility,
chromatin modifications and so on and so forth.

Our eyes have been opened to the vast complexities of transcription
regulation.

Our current understanding, simplified, is that some proteins bind on
somehow specific locations on the DNA molecule and help regulate gene
transcription. They do this through complex interactions with other such
proteins that bind on proximal or distal positions, or proteins that
don’t directly bind DNA but are nevertheless important for the
interactions. The way that all this influences gene transcription is
through the facilitation or obstruction of the binding of the proteins
that actually transcribe DNA (transcriptional machinery), on the
locations where they need to bind in order to start the transcription.

This in essence turns transcription on or off and controls the gene’s
production.

<span id="anchor-2"></span>Trans-Regulation

<span id="anchor-3"></span>Binding

We call these DNA-binding, transcription-regulating proteins
Transcription Factors (TFs).

TFs bind to DNA through physical interactions between the amino acid
side chains of the protein and the accessible edges of the base pairs of
the DNA molecule. Those include direct hydrogen bonds, water-mediated
hydrogen bonds and hydrophobic contacts. \\cite{Slattery2014}

We can categorize proteins based on the domains (protein fragments that
can fold correctly without the rest of the polypeptide) that they
consist of. TFs contain domains that make contact with the DNA molecule
by presenting a protruding surface and/or a flexibly extended structure
\\cite{harrison1991structural}. We call these ‘binding domains’ and they
are a useful way of classifying TFs into families.

+++

the C2H2 zinc-finger, homeodomain, and helix-loop-helix dominate in
humans and mouse \\cite{Vaquerizas2009}

The other partner of this interaction, the DNA molecule, also presents
itself in a variety of shapes and is not always found in its
bibliographical B-form double helix form \\cite{Joshi2007}.

<span id="anchor-4"></span>Binding sites

In order to study trans regulation, a common first question is “where
does protein X bind on the genome”. Between the various shapes of both
the TFs and the DNA molecule, various emerging features seem to
contribute in TF-DNA readout, on multiple levels.

First and foremost, at least in terms of frequency of use in research,
is the strong preference for specific genomic sequence patterns that
many TFs display

{stormo2000, Bussemaker2007,
Badis2009diversity,stormo2010determining,Weirauch2013evaluation,
jolma2013dna, whitte2013massively}.

Every base pair has a unique hydrogen bonding signature (in the major
groove, not in the minor) making it intuitive to imagine a sort of
“alphabet” or code on the genomic sequence that TFs would recognize,
much like tRNA molecules recognize trinucleotides. In combination

with observations derived from three-dimensional structures of
protein-DNA complexes \\cite{rohs2009the}, the base-readout mechanism of
TF-DNA binding is very well established, but is nevertheless not the
entire picture.

In some cases, the nucleotide sequence offers binding opportunities not
by hydrogen bonds on the major groove, but rather by conforming the DNA
molecule such as in the opening of the minor groove in the complex
formed between TBP and the TATA box
\\cite{rohs2009the\_7,rohs2009the\_78}. In another case, a Drosophila
Hox protein is shown to bind to particular minor groove narrowings which
emerge as sequence specific characteristics and create a local dip in
electrostatic potential \\cite{Joshi2007}.

Some TFs form complexes with non-binding factors (co-factors) which can
change the binding affinity of the complex \\cite{siggers2011,
slattery2011cell}. Others need to be bound in multifactor complexes
\\cite{enhanceosome}.

Most of the DNA is usually found in a tightly packed chromatin state,
conceptually occupied by nucleosomes. Histones, the protein parts of
nucleosomes are the most important DNA binding proteins and their
dynamics and their effects on gene regulation are complex enough to
warranty a section later on in this work.

Some factors are heavily deterred by nucleosome occupancy
\\cite{liu2006Leu3, Bai2010,Kaplan2011} while others compete with
nucleosomes \\cite{polach1996, barozzi2014}, potentially interacting
with them \\cite{glat2011, zaret2011, PIQ }.

Some factors contain multiple independent DNA binding domains

https://academic.oup.com/nar/article/42/4/2099/2435233

Besides all that, the DNA might be methylated which can inhibit TF
binding \\cite{Domcke2015}.

Unfortunately then, answering the “where does it bind” question is
anything but simple.

<span id="anchor-5"></span>Wet lab techniques

The identification of TFbs has relied heavily on Structural biology and
more recently in high-throughput technologies such as CHiP-seq.

The Encyclopedia of DNA Elements (ENCODE) Consortium, an international
collaboration of research groups with the goal of cataloguing the
functional elements of the human genome, tackles the problem of
identifying TFbs with CHIP-seq assays against TFs (listing about 2500
such assays in human context so far).

This technique is the currently best wet-lab approach to answer the
question: “In this biological sample, which parts of the genome are more
likely to be bound to protein X” with direct evidence.

With Chromatin-immunoprecipitation sequencing (CHip-seq), we can detect
regions on the genome that interact with a protein of our choice (for
which an appropriate antibody must be available). Very briefly, we
immunoprecipitate out protein while it’s still bound to DNA and then we
sequence the DNA fragments that we collected. The sequenced fragments
are them ampped on a reference genome. The regions of the reference
genome where we detect more reads than expected are the regions of the
genome that were touching our protein in vivo. The results of such an
assay, after some statistical analysis, offer regions ( that can range
in size up to 300bp or more ) where we have high confidence that our
favorite protein binds.

For some questions that a researcher might have, this resolution will be
limiting. To increase the resolution and have a single-base-resolution
predictions, we combine our direct-evidence data with computational
methods.

<span id="anchor-6"></span>Dry lab techniques

Computational methods model the TF-DNA interactions, using knowledge
gained by direct-evidence experiments, and then apply the model in order
to offer predictions on conditions for which direct-evidence data is not
available.

The goal of any such approach is to reach the predictive power of a
molecular biology assay but of course this is, for now, impossible.
Nevertheless, these techniques are integral to modern regulation
analysis since they can be applied essentially for free in comparison to
biological assays and even though the results are technically imperfect,
they are still very useful for biological predictions of high quality,
especially when used in tandem.

<span id="anchor-7"></span>PWMs

The most common such computational model are Position-Weight Matrices
(PWMs). This model exploits the observation that many TFs have a strong
preference for specific genomic sequence patterns \\cite{Slattery2014}.

A PWM then, models a certain number of bases and contains a weight
-which can be determined in a variety of ways- for each of the 4
nucleotides in each of those positions \\cite{stormo2000}(a weight per
base for each position: a position weight matrix). These simple
mathematical objects can lead to very fast sequence searches
\\cite{Claverie96} and as a consequence have been used extensively to
identify putative sites of TFs. In fact, often, they are found as the
basis of more complex computational methods.

In comparison to consensus sequences, an earlier approach to this
concept, PWMs handle mismatches in a more plastic way, since they can
score each base in each position separately instead of relying in a
perfect match or not, but there are drawbacks in this model as well. For
example, PWMs incorrectly assume that each position contributes
independently in the binding and they don’t account for position
interdependencies {Mathelier2013}.

<span id="anchor-8"></span>Other seq-based approaches

Plenty of effort has been invested in improving or surpassing PWMs. From
improving the PWM model {Sharon2008, Gersh2005}, to alternative models
such as dinucleotide weight matrices {2nucWeightMatrices}, alternative
heuristic approaches{Mathelier2013}\[20,21,22\], hidden Markov models,
variable-order Bayesian networks or the TF Flexible Model
{Mathelier2013}.

Despite the plethora of alternatives, no method has managed, yet, to
replace PWMs as the basic model to detect single base resolution TFbs.
PWMs are improved over time, as new direct-evidence data is collected,
they are fast to implement and very intuitive thanks to the ability to
represent them as logos.

<span id="anchor-9"></span>Neural Networks

Artificial Neural Networks (NN) are enjoying an era of great development
and popularity, thanks to advancements in technology, namely the
increased computational power of Graphics processing units(GPU) and
landmark papers such as {Krz2012} that firmly established NNs at the
forefront of computer vision, artificial intelligence and machine
learning. A few years into this explosion of NNs, NN-based programs are
the best chess-playing algorithms and have managed, for the first time,
to beat human champions in the game of Go, one of the last bastions of
human intellectual supremacy over the machine.

Nevertheless, NNs are not a new idea. The theoretical base was already
explored in the 19th century. In fact, the perceptron algorithm, a
simple artificial NN was at the core of the development of
PWMs{perceptron, stormo2000}.

Artificial NNs are heavily inspired by real, biological neural networks
but in the context of computational biology, at least in this body of
work, they should be considered as nothing more than a very complex,
impressively capable and efficient, linear algebra model. What can be
abstracted as neurons for the benefit of human intuition, is distilled
in large matrix operations. That’s why NNs resurged after sufficiently
powerful GPUs were developed; GPUs are exceptionally good at handling
big matrix operations (this is what computer graphics boils down to as
well).

<span id="anchor-10"></span>How NNs work

Should I get into more detail ?

NNs quickly became the bleeding edge of TFbs analysis.

The first layer of a typical such model “discovers” (learns) many dozens
of PWMs (features of W length and 4 height) and when presented with a
sequence it will ‘run’ those PWMs (features) across the sequence and
output their score for each possible successive position on the
sequence.

If approached from a different point of view, each of those possible
successive positions, can be considered as a neural node, that receives
input from W points of the sequence and outputs one score for each of
the features (PWMs) that it is learning about.

The second layer of a NN learns PWM-like features as well, but this time
they are based on the output of the first layer. It learns “PWMs of
PWMs” (higher order features). From the alternative point of view, a
second layer of neurons are connected not to the raw sequence but on the
first layer of neurons ( and in a very orderly fashion at that ).

This second layer is a huge boost to the complexity of the model since
it can capture dynamics such as varying distance between features, or
“either this feature or that feature in X position”.

As was hinted in the previous paragraphs, a NN for genomic sequences
will typically take as input a large sequence. While a big PWM might be
modeling 30bps, a NN will model from 300-1000bps. This allows the NN to
incorporate a much wider and broader sets of features since the sequence
itself sometimes contains information about nucleosome occupancy or DNA
shape, DNA features which as was discussed already play a role in TF
occupancy.

NN papers:

**DeepBind**: pattern discovery:
https://www.nature.com/articles/nbt.3300

**Deepmotif**: This paper applies a deep convolutional/highway MLP
framework to classify genomic sequences on the transcription factor
binding site task.
[*https://arxiv.org/abs/1605.01133*](https://arxiv.org/abs/1605.01133)

**CKN-seq: **compares to deepBind:
https://www.biorxiv.org/content/early/2017/11/10/217257

**TFimpute:** The proposed model, termed TFImpute, is based on a deep
neural network with a multi-task learning setting to borrow information
across transcription factors and cell lines.:
https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1005403

**Kitchen-sinks**: prediction of transcription factor binding sites from
DNA sequence. https://arxiv.org/abs/1706.00125

**DeepSea**: https://www.nature.com/articles/nmeth.3547

**DanQ**: a hybrid convolutional and recurrent deep neural network for
quantifying the function of DNA sequences
[*https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4914104/*](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4914104/)

<span id="anchor-11"></span>Other non seq-based approaches

A drawback of the class of models that, like PWMs, model binding based
only on the genomic sequence is that they are context-ignorant. As was
noted earlier, the spatially and temporally specific binding of TFs on
DNA is very crucial to cell differentiation and the binding sites of a
factor will change drastically between different cell contexts but a
sequence-only model’s answer will always be the same (**TFimpute
**actually tries to tackle exactly this issue).

In order to add contextual information, we can use other models in
tandem with our sequence based one. For example we can use a model that
models DNA-shape based on sequence and use its output to better inform
our TFbs predictions. A better alternative is to use some wet-lab
experiment’s data such as genome-wide data for nucleosome occupancy or
DNA-methylation levels.

A very interesting option for such an endeavor are the DNA-accessibility
assays DNAase and ATACseq (see later chapters). The two methods are very
similar, with ATACseq being newer faster and cheaper. In these assays,
we introduce a DNA-cutting protein to our biological sample, and after
sequencing we can deduce in which regions of the genome the protein made
a cut most times. Regions of the DNA molecule that are tightly packed
and inactive will not be accessible to the DNA cutting protein but in
regions with regulatory activity, the DNA molecule is much more
accessible and vulnerable. We use these techniques to identify
accessible regions which we treat as a proxy to regulatorily active
regions. The identification of regulatory-active regions is already a
great step for this analysis as we can already safely discard any
putative TFbs that fall outside of those regions.

Besides this though, interestingly, many DNA-binding proteins also
protect the underlying DNA from the cutting activity and consequently
leave a “footprint” on the DNAse/ATACseq signal, a depression of signal
in an otherwise active region.

A few models have been developed to take advantage of this phenomenon.

The Wellington algorithm models an imbalance in the DNA strand-specific
alignment information of DNase-seq data surrounding protein–DNA
interactions to predict occupied TFbs and in does so differentially to
identify cell-type determining TFs.

Centipede applies a hierarchical Bayesian mixture model to infer regions
of the genome that are bound by particular transcription factors. Two
other approaches, msCentipede and Romulus, take further steps to improve
on the idea. {centipede}.

PIQ uses machine learning techniques to model the magnitude and shape of
genome-wide DNase profiles and identify occupied TFbs.

This approach of modeling the DNA accessibility data instead of the DNA
sequence has its own drawbacks. For example, different TFs have
different occupancy times and strengths and as a result some TFs don’t
bind DNA long enough, or strong enough or in enough cells in our
biological example to leave a mark on the accessibility signal.

<span id="anchor-12"></span>Nimrod

If not all, then most of the computational approaches to identify
occupied TFbs that we mentioned so far, rely on PWMs and work by
scanning the genome for PWM hits and then filtering those based on each
model’s parameters. We further divided them in two other categories, the
models that rely only on genomic sequence and those that rely on DNA
accessibility data. The later might still rely on PWMs as “seeding” but
to our knowledge no method integrates genomic sequence and accessibility
signal in the same model.

We developed a NN to attempt such an approach. Our model was designed a
lot like the NNs mentioned earlier. A first layer (seq layer 1) takes
the genomic sequence (a total of 1000b around a PWM hit) as input and
outputs to the second layer (seq layer 2) which learns higher order
features. At the same time, two layers with the same configuration
operate on ATACseq signal from exactly the same genomic region (atac
layer 1 and 2). These layers will learn features of ATACseq signal just
like the sequence layers learn sequence features.

The two inner layers (seq layer 2 and atac layer 2) get ‘zipped’/merged
into a single layer and the now merged signal gets passed to more layers
of the model for even higher order feature discovery. The signal finally
converges to the output layer of the NN which offers a score for the
region that is tested.

We present an implementation of our model, made with TensorFlow and
accompanying python code to facilitate the analysis and generation of
new models.

We trained our model in a variety of biological contexts and compare it
with the strongest of the previously mentioned models to show how our
model outperforms them.

\#\#

Predicting transcription factor binding *in vivo* is more difficult
because it is affected by other proteins, the chromatin state and the
physical accessibility of the binding site.

\#\#

<span id="anchor-13"></span>Epigenetics

One of the first debates in biology was about the way with which complex
organisms develop. An early popular theory was that organisms are
preformed, perhaps existing as little homunculi inside sperm, and then
simply grow during development. Preformationism lasted as the dominant
theory until the end of the 19th century or early 20th, by which time
enough evidence had been gathered to show that actually organisms
develop based on a plan that is orchestrated by nothing more than
complex chemical reactions, a process that would be called “epigenesis”
{Felsen14}.

The discovery that genes, as they were understood at the time, could be
associated to specific regions of chromosomes solidified epigenesis as a
leading theory([*Morgan
1911*](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3941222/#A018200C42))
and the identification of DNA as the main information-holding molecule
changed the landscape even further.

In 1970 Laskey and Gurdon ([*Laskey and Gurdon
1970*](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3941222/#A018200C31))
show that the DNA of a somatic cell nucleus was competent to direct
embryogenesis when introduced into an enucleated egg. That means that
somatic cells inherit the entirety of the genomic information of the
zygote. Yet the

divergent phenotypes between differentiated cells and the communication
of those to daughter cells were also undeniable. The information is in
the DNA, but different cells have the same DNA, so how are they
different? This is the burning question behind the term epigenetic, as
it is understood today. Riggs et al, in 1996 defined it as “the study of
mitotically and/or meiotically heritable changes in gene function that
cannot be explained by changes in DNA sequence” ([*Riggs et al.
1996*](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3941222/#A018200C59);[
](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3941222/#A018200C58)[*Riggs
and Porter
1996*](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3941222/#A018200C58).

<span id="anchor-14"></span>Methylation

As outlined by {Felsen14}, DNA methylation was one of the first
epigenetic (in the modern definition) processes to by studied. Although
the idea that DNA methylation might play a role in DNA regulation was
not completely new at the time, Riggs in 1975 ([*Riggs
(1975)*](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3941222/#A018200C57),
based on findings about methylation in bacteria, proposes a model of DNA
methylation regulated inactivation of the X chromosome. Later work would
confirm, refine, enrich, and extend the model to explain how it
epigenetically controls the activity statuses of genes.

DNA methylation is a process by which methyl groups are added to the DNA
molecule, catalyzed by DNA methyltransferase enzymes
(DNMTs){Bogdanovic2009a}. The modified cytosine can be found in all
vertebrates and flowering plants; some fungal, invertebrate, and protist
taxa; and many bacterial species {bestor05}. Nevertheless the dynamics
of methylation appear to have different importance and roles in
different branches. In vertebrates, methylated cytosine is almost
exclusively found on the cytosine of CG dinucleotides (CpG) with 60 to
90% of all the CpGs being methylated in mammals (Bird 1986) (82% for
human ) whereas in insects this percentage drops to under 1% {wang14}.

Methylation of gene bodies is an important pattern that has been found
to be conserved between plants and animals {zemach10} and is correlated
with higher gene expression in plants, invertebrates and vertebrates
{wang14}. On the other hand, de-methylation of promoters is considered
an important activation mark and correlated with increased transcription
levels (but not in invertebrates {zemach10}).

Additionally, plants and vertebrates (but not invertebrates) display
high levels of enrichment on transposable elements which is understood
to be a defensive mechanism (via repression of the element) against the
potentially harmful effects of those elements.

+++ Ozren “demethylation at enhancers“

DNA methylation is critical for the proper development and health of
individuals. This alteration of the DNA molecule can influence gene
expression in at least two ways. One is to interfere with TF binding by
making the methylated site inhospitable to the TF. The second way is by
recruiting proteins that are associated with chromatin modifiers to make
the chromatin environment even more repressive {Bogdanovic2009a}.

*These proteins read and interpret the epigenetic signals and provide a
connection between DNA methylation and chromatin modification.*

<span id="anchor-15"></span>

<span id="anchor-16"></span>

<span id="anchor-17"></span>Histone modifications

The chromatin modifiers that were just mentioned, might include a
category of enzymes that are responsible for a range of
post-translational modifications of the most abundant DNA-binding
proteins; the histones. The DNA molecule is packed tightly in the
nucleus of eukaryotes, wrapped around octamers of the “core” histones
(one copy of H3-H4 tetramer and two copies of H2A-H2B dimer) whose
amino-terminal tails pass over and between the the DNA superhelix to
contact neighbouring particles{Luger97}.

Most nucleosomes also recruit either histone H1 or high mobility group
(HMG) proteins (sometimes both) which bind to the outside of the
nucleosome to form a particle known as the chromatosome{COCKERILL}.

The first connection of histone modifications to gene regulation was
proposed surprisingly early in 1964 when, with a working model that
considered histones as proteins that bind to DNA and repress RNA
transcription,[*Allfrey and
Mirsky*](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3941222/#A018200C3)
presented their findings that acetylation and methylation of histone
takes place post-translationally and that acetylation specially “may
affect the capacity of the histones to inhibit ribonucleic acid
synthesis in vivo”{ALLFREY64}.

Since then, the nucleosome was described and a lot of the details of the
above model were illuminated. The N-terminal tails of histones, where
most modifications take place, can undergo a large variety of
modifications (at least eight distinct types) but acetylation,
methylation and phosphorylation have dominated scientific interest
{kouz07}. Besides modifications of the N tails, reports of modifications
located within the globular domain of histones are surfacing
{Kebede2015}.

In the ‘beads on a string’ model, the DNA molecule is first wrapped
around histones and then wrapped into higher order structures
facilitated by the histones. The nucleosomes can now be thought of as a
sequence of units which, besides the raw genomic sequence, also vary in
nucleosomal structure, thanks to modifications. The beaded string now
forms a new ‘primary structure’ of chromatin which can give rise to a
multitude of higher order structures{Luger12}.

The tight packaging essentially blocks access to the DNA molecule, which
is a double edged sword. On one hand, foreign DNA such as retroviral
elements are silenced. On the other hand, of course, the cell needs
access to the DNA for many processes. To overcome this, cells employ
specialized chromatin remodeling complexes which either reorganize
nucleosomes directly in an ATP dependent manner, or toggle modifications
on the histone tails which can alter altering nucleosome structure,
stability and dynamics. {zheng13}

To study these modifications in the modern context, we typically perform
CHip-seq assays with antibodies that recognize the modified histone.
This isolates the fragments of DNA that were in proximity to the target
histone. We then sequence the fragments and map them on a reference
genome to acquire genomic tracks of said modification. Regions of the
genome that harbored our target modified histone in a significant
percentage of the cell population of the sample will give a stronger
signal in the sequencing experiment.

\#https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3941222/

\#http://science.sciencemag.org/content/356/6335/320?casa\_token=VweihhvA\_TsAAAAA%3A7aJdHMvhDszaGKXZg6STcEYLeQDeeOKPUabD\_IzH43Qy\_YjjnktI5\_ibe9jGQKr18u4CZG3A-wM

\#HIstone modifications

**\#1966:** RNA synthesis and histone acetylation during the course of
gene activation in lymphocytes.

\#
[*https://www.ncbi.nlm.nih.gov/pmc/articles/PMC224233/*](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC224233/)

\# {COCKERILL}. Has an amazing figure on the nucleosome

<span id="anchor-18"></span>H3k4me3

The thi-methylation of the fourth lysine of histone 3, one of the post
translational modifications of histones that were just discussed, was
shown to be mediated by the Set1 protein in yeast and in contrast to the
di-methylated state, which is mediated by the same protein, was found
exclusively at active genes{santosrosa02}. In fact it appears that, at
least in yeast, the establishment of H3k4me2 is untargeted and genome
wide whereas H3k4me3 is established in a targeted and controlled
manner.{ngetal03}

Similar patterns were observed in higher eukaryotes but not without
differences. Specifically in chicken, in was observed that while H3k4me3
can be used to classify genes in active and inactive and preferentially
associates with the transcribed regions of genes, it can still be
detected in lower quantities on inactive genes{Schneider03}.

In humans, h3k4me3 is mediated by Set9, which competes with histone
deacetylases and precludes the H3k9 methylation by Suv39h1{nishi02}. Up
to 75% of our genes were found to be tri-methylated even without
detectable RNA elongation {guenther07}.

Nevertheless, H3k4me3 was one of the epigenetic marks most employed by
the ENCODE project in its attempt to identify functional elements on the
human genome (see more in following chapter). At the time of writing,
168 different chip-seq assays against H3k4me3 are available for human
cells and tissues on the ENCODE project’s page and 106 for mouse. These
assays were conducted in order to characterize promoter regions (again,
see more in following chapter). It is believed that nucleosomes that are
modified by H3k4me3 form a chromatin state that somehow facilitates
transcription initiation.

In zebrafish too, H3k4me3 has been shown to be enriched at
transcriptional start sites, an association which correlates with gene
expression {aday11, Bogdanovic2012}.

We contribute to the effort to investigate this modification by
providing H3K4me3 assays in two new developmental stages of zebrafish,
significantly enhancing the resolution of the available data during the
development of the popular model organism.

<span id="anchor-19"></span>H3k27ac

As we discussed earlier, trans-acting regulatory proteins bind on DNA in
sites both proximal and distal to TSSs. The regions of these distal
binding sites usually behave as ‘hotspots’, offering binding sites for
multiple TFs. We can think of those regions as functional units of the
gene regulation system and we can categorize them depending on further
characteristics such as chromatin state, or chromatin accessibility (see
later chapter).

H3k27ac is another heavily investigated histone modification (ENCODE: 97
in human, 93 in mouse). Acetylation has long been associated with
transcriptional activation, in contrast to histone deacetylation which
is generally thought to have roles in transcriptional
repression{zheng13}.

Originally investigated in yeast {o\_k27ac\_yeast} and later in human
and mouse {o\_k27\_human}, H3k27ac was initially reported to be highly
enriched at promoter regions of transcriptionally active genes
{wang2008} but was later established as an important mark to detect
active enhancers{creyghton10, radaiglesias11, bonn12}.

H3k27ac has also been investigated in early zebrafish development where
it was shown to positively correlate with gene expression
{Bogdanovic2012}.

In this work, we present H3K27ac assays in two new developmental stages
of zebrafish, significantly enhancing the resolution of the available
data during the development of the popular model organism.

\#\#???

This feels a bit weak, i want to say more about distal elements but i
also want to save it for later chapters

Other papers?

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3003124/

<span id="anchor-20"></span>Chromatin accessibility

We have discussed how non-coding regions of the genome can be marked as
cis-regulatory elements in various ways, depending on the particulars of
each investigated case. Regions can be marked by epigenetic marks such
as methylation or histone modifications, but there is an important
aspect of our model that has not yet been addressed by our approaches so
far.

Our model requires that TF proteins bind the actual DNA molecule.
Histone modification assays give us information about the state of the
histone proteins, but we still don’t know if the DNA is actually
accessible or still wrapped tightly around the histones, however they
are modified.

This is a question that can be answered with a different class of
biological assays that offer us a genomic signal that directly measures
to what degree the chromatin is accessible to a protein.

Deoxyribonuclease I (DNase I) is an endonucleases \\footprint{ Enzymes
that cleave the phosphodiester bond within a polynucleotide chain} that
cleaves DNA relatively non specifically (although some biases have been
identified. {FIND DNAsE BIAS PAPER}). In DNase-seq {og\_dnaseseq}, DNase
I is exploited to detect regions of the genome that are hypersensitive
to it and thus, accessible. Using DNase I to determine chromatin
accessibility is an old trick {old\_dnase}. The principle was further
refined in FAIRE-seq and ATAC-seq{og\_atacseq}.

These techniques have been widely employed in a plethora of contexts
that are too many and varied to be listed here.

767 DNAse-seqs on ENCODE

<span id="anchor-21"></span>ATACseq

With ATAC-seq (Assay for Transposase-Accessible Chromatin), we exploit
another DNA cutting protein, the transposase Tn 5. Transposases are
proteins that “copy-paste” or “cut-paste” genes (transposons, or
transposable elements). This ability of transposons ( which often encode
for their own transposase ), allows these elements to evolve on a
separate plane than their host organisms, like viruses or parasitic
microorganisms. Their effect on the genome is inherently mutagenic
though, so transposons cannot be too aggressive otherwise they will kill
their host and die themselves. Nevertheless, transposases are the most
abundant genes in nature {abundand\_transposases}.

In fact they are often used by the hosts {....}

The Tn5 protein is actually a retro-transposase, a family of proteins
used by viruses to insert viral retro-transcribed DNA in bacterial
genomes.

It works as a dimer, two molecules attach and cleave DNA at specific
19bp sites that surround the transposon. The two parts of the dimer come
together to form a homodimer, merging the two cuts on the “main” DNA
molecule and removing the intermediate piece which is now bent like a
hairpin. The reverse process then follows in a different part of the
genome. The dimer cleaves the DNA and inserts the intermediate
hairpinned DNA, potentially reversed.

We can “load” Tn5 in vitro with DNA of our choosing as long as it
contains the 19bp binding sites that the transposase expects. We can
make small DNA pieces that include both these 19bp sequences and the DNA
barcodes that are required by next generation sequencing processes.

The pieces don’t need to form a hairpin, each part of the dimer will
attach to a DNA piece and a cut will be created in the host’s DNA after
the insertion.

As was discussed, transposases cannot be too active and Tn5, true to
form, has very low activity in nature. The protein used in molecular
techniques had its activity amplified through a series of mutations
{tn5\_mutations}.

Exposing host DNA to a high concentration of these preloaded Tn5
proteins will result in the concurrent cleavage of the genome in the
regions where the DNA molecule is accessible {tagmentation}. The
fragments will also have sequencing barcodes \\footprint{and also those
19bp tn5 target sequences, those are there too} at their ends, which the
transposases inserted. They can now be directly sequenced and mapped to
a reference genome, giving us a genome wide signal of DNA accessibility.

ATACseq is a very attractive experiment for the simplicity of execution
and richness of data it provides. Besides allowing us to determine
accessible regions of the chromatin, the nature of the sequencing reads
allows us to infer nucleosomal positions and detect TF binding sites
(see previous chapter?).

In this, we work extensively with ATACseq data generated on whole
embryos during the early development of zebrafish and amphioxus (see
later chapters), generated by labmates and collaborators.

<span id="anchor-22"></span>A bit on TADs

<span id="anchor-23"></span>Cis-Regulation

The TFbs which we talked about in the previous section are most often
found within genomic regions that can be interpreted as functional units
of the gene regulation system. These *cis *elements (cis regulatory
elements CREs) only drew scientific interest after the period of “Modern
Synthesis” but by now are recognized as important players in the
evolution of organisms.

In fact, their effect on the expression of developmental genes
distinguishes them as the major drivers of animal form evolution
{Maeso2016a}.

CREs have traditionally been categorized based on their distance to a
gene’s transcription start site and their observed effect on the
transcription levels of their target gene. Elements laying just upstream
of the TSS of a gene are called promoters and distal elements are split
into enhancers and silencers. Further nuanced names can be used
depending on the analysis used such as ‘poised enhancers’ or ‘active
enhancers’.

A growing number of studies suggest that there appears to be very little
difference between promo- ters and enhancers, challenging the
established view separating them as distinct entities (reviewed in
\[48,49\]).

Such that have the capacity to drive transcription independent of their
target promoters{Kim2015}

The roles of promoters and enhancers in transcription have been thought
to be distinct; however, these two regulatory elements are highly
interrelated and show noticeable similarities in structure and function.

On top of that, when investigating cis regulation it is a good idea to
take all the cis regulatory elements into consideration. That would
include promoters, enhancers, silencers, but also other elements such as
CTCF-binding sites which are typically investigated as a distinct
unit{ctcfloops}. All these combine to form the regulatory landscape of a
gene.

Since we will work a lot with ATACseq, will will mostly be dealing with
ATACseq peaks (regions of statistical enrichment) which should be all
the regulatorily active regions of the genome. Nevertheless it is wise
to be aware of various subcategories of those and their potential
particularities.

<span id="anchor-24"></span>A bit on promoters

Perhaps the most straight forward CRE to talk about are promoters. They
are after all the most ancient and basic of elements, being found in all
subdivisions of life, including viruses.

We call promoter the genomic region that starts at a gene’s TSS and
extends upstream and on the same strand for an arbitrary length of DNA,
usually some several hundred base pairs {lenhard2012metazoan}. Promoters
contain TFbs, sometimes with very deep evolutionary conservation (TATA
box, initiator), specially so in the ‘core promoter’ region, the region
directly adjacent to the TSS.

It is here, on the gene’s promoter, that the regulatory complexity
*collapses *\\footnote{ if we are allowed to borrow the term from
quantum physics without fully understanding it } into whether or not the
appropriate RNA polymerase will bind and begin transcribing the gene or
not.

The metazoan promoters, perhaps as a natural consequence of having to
integrate more complicated regulatory information are more complex
{lenhard2012metazoan} than those of bacteria or single-cell eukaryotes.

Typically we can assign one promoter per (protein coding ?) gene,
although bidirectional promoters are not too uncommon.

Although proximal promoters may not contain all of the information
required to precisely control transcription of individual genes in time
and space during development, analysis of promoters alone can generate
meaningful models of transcriptional regulatory networks. {Carninci2006}

Because of the very direct and obvious connection of any gene to its
promoter (the promoter lies upstream of the TSS), promoters are the
easiest class of CREs to identify and assign to a gene; we just need to
know where the TSS of the gene is and look X number of bases upstream.
From there we can look for TFbs in the promoter region and assign them
to the gene’s regulatory network.

If one is interested in identifying only transcriptionally active
promoters, a CHIPseq assay agains h3k4me3 is typically used nowdays

<span id="anchor-25"></span>A bit on enhancers

Distal elements have been noted to enhance or repress the transcription
levels of their target genes and thus are often categorized as enhancers
and silencers. Enhancers have attracted more scientific interest, and
thanks to their discoverability via histone modifications (see previous
chapter on H3k27ac) are simple to identify.

\# review on types of elements

http://bejerano.stanford.edu/readings/public/10\_Intro\_TxRegReview.pdf

core promoters, proximal promoters, distal enhancers, silencers,
insulators/boundary elements, and locus control regions

CREs that are not directly adjacent to a gene’s TSS are not as easy to
detect on a genome and so far, researchers have relied on a variety of
methods to detect them. Some distal elements are conserved between close
or sometimes even distant species and thus can be identified by genome
comparisons {bofelli04, Sandelin04r, kikuta07 }.

Other approaches rely on scanning the genome for TFbs (via PWM hits) and
then detecting regions with statistically denser concentrations of TFbs
(reviewd in Aerts, 2012). Binding events outside of regulatory elements
are less likely to have a significant impact on transcription
{ballester14} so it makes sense to focus our effort in the regions with
many (putative TFbs). ++In modern version of the same genomic modeling
approach, neural networks are trained to not only predict TFbs but
directly predict CREs ++

In other cases, CREs were discovered based on GWAs and the
identification of SNPs outside of coding regions ++. This discovery that
a mutation on a non-coding sequence (typically on a TFbs) can lead to
disease firmly established CREs as an important target for future
therapeutic research{Creyghton10}.

Once a CRE has been identified, through any computational or even
biological way, it can be tested with transgenic reporter assays
(reviewed in {kvon15} ). In those analyses, we typically insert our
putative enhancer sequence in the host genome, next to a reporter gene.
When properly incorporated in the genome, this inserted DNA will produce
our reporter gene only in the contexts (tissue/developmental stage) in
which our enhancer is active. Similar methods have been developed for
the analyses of silencer elements{citsZe}.

<span id="anchor-26"></span>Recap?

<span id="anchor-27"></span>Under the light of Evolution

Paragons of the natural sciences such as Aristotle or Pliny invested
significant parts of their lives in the study of animals and their form.
It is after all the simplest, most intuitive first step towards studying
life. Unfortunately comparative anatomy would only be revived only after
the comma of ecclesiastical absolutism of the middle ages. We urge the
reader to read E. W. Gudger’s 1934 paper on renaissance and the pioneers
of ichthyology in the early 16th century{Gudger1934}. Among them,
**Pierre Belon** who among other works published a comparison of a human
to a bird skeleton, is singled out as the “prophet” of modern
comparative anatomy.

By the time Darwin was publishing the origin of species, the concepts of
homology and analogy were well understood by his contemporaries. That
was before the general acceptance of phylogeny {Panchen1999}. As the era
of molecular biology dawned, the concept of homology was expanded to
refer to proteins, which were initially investigated based on their
chemical similarities{fitch70} and later on based on their sequence and
like so, we started talking about homologous genes.

We quickly established that most evolutionary relationships that had
been established based on morphology were confirmed on the genomic
level. Some details were clarified, such as the edge of the primate
tree.

We discovered that the coding sequences of genes display remarkable
conservation in the tree of life. Some genes go all the way back to..

When comparing the coding sequences between human and mouse, the
majority of proteins were found to be identical {CCDS}

Yet the two organisms display radically different morphology

Which brings us back to our point that form evolves more though
transcription regulation rather than protein form.

As we discussed earlier, cis-regulation research is a newer discipline
when compared with other genomic research areas and so many mysteries
remain unsolved. Recent work has shown that cis regulatory elements have
a higher turnover rate than coding regions.

Human to mouse comparisons provide us with interesting cases of
conserved regulation{++}, some of them connected to human disease
{which?} or developmental problems {tad border polydactyly}. However
while we can detect conservation of coding sequences between human and
organisms as evolutionarily distant as bacteria (right?), efforts to
detect conserved cis-regulation between human and fly or caenorhabditis
did not yield the expected results.

<span id="anchor-28"></span>Amphioxus

<span id="anchor-29"></span>Zebrafish

The evolution of vertebrate cis regulation

{Bogdanovic2012}:

Indeed, the zebrafish (Danio rerio) model system was recently employed
in an H3K4me1/me3-based genome-wide screen to identify putative
cis-regulatory features in a vertebrate embryo (Aday et al. 2011).

*CRE sequence and evolution*

Changes in the sequence of CREs have the potential to change the
expression of one or more genes and consequently have been implicated in

&gt; widespread and systematic localization of variants associated with
a wide spectrum of common diseases and traits in regulatory DNA
\\cite{cres\_inDisease}

&gt; and of course cancer \\cite{ dipl\_cres\_inCancer }

&gt; Deletion of both ZRS and shadow ZRS abolishes *shh* expression and
completely truncates pectoral fin formation. \\cite{ joaq\_pectoralFin }

&gt; \\cite{martin\_review} \# anything cool to cite from here?

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1914805/pdf/ajhg00023-0023.pdf

**Chordates Vertebrates**

chordates share a fundamental bodyplan that was greatly elaborated in
vertebrates

All vertebrates share multiple morphological and genomic novelties

 1 Bertrand, S. & Escriva, H. Evolutionary crossroads in developmental
biology: amphioxus. Development 138, 4819-4830 (2011).

 2 Janvier, P. Facts and Fancies about Early Fossil Chordates and
Vertebrates. Nature 520, 483-489 (2015).

**WGD**

 the most prominent genomic difference from non-vertebrate chordates is
the reshaping of the gene complement that followed two rounds of whole
genome duplication (WGD or 2R), that likely occurred at the base of the
vertebrate lineage

 3 Dehal, P. & Boore, J. L. Two rounds of whole genome duplication in
the ancestral vertebrate. PLoS Biol 3, e314 (2005).

 4 Putnam, N. et al. The amphioxus genome and the evolution of the
chordate karyotype. Nature 453, 1064-1071 (2008).

 These large-scale mutational events are hypothesized to have
facilitated the evolution of vertebrate morphological innovations, at
least in part through the preferential retention of ‘developmental’ gene
families and transcription factors (TF) after duplication

 4

 5 Holland, L. Z. et al. The amphioxus genome illuminates vertebrate
origins and cephalochordate biology. Genome Res 18, 1100-1111 (2008).

 However, duplicate genes and their associated regulatory elements were
initially identical, and could not drive innovation without regulatory
and/or protein-coding changes.

 To date, the impact of vertebrate WGDs on gene regulation remains
obscure, both concerning the fates of duplicate genes and the
acquisition of the unique genomic traits that are characteristic of
vertebrate genomes. These traits include numerous features often
associated with gene regulation, such as unusually large intergenic and
intronic regions

 6 Lander, E. S. et al. Initial sequencing and analysis of the human
genome. Nature 409, 860 - 921 (2001).

 7 Nelson, C., Hersh, B. & Carroll, S. The regulatory content of
intergenic DNA shapes genome architecture. Genome Biology 5, R25 (2004).

 a distinct set of highly conserved non-coding regions (HCNRs)

 5

 8 Vavouri, T. & Lehner, B. Conserved noncoding elements and the
evolution of animal body plans. Bioessays 31, 727-735 (2009).

 and high global 5-methylcytosine (5mC) content and 5mC-dependent
regulation of embryonic transcriptional enhancers

 9 Bogdanović, O. et al. Active DNA demethylation at enhancers during
the vertebrate phylotypic period. Nat Genet 48, 417-426 (2016).

**1. Fate after duplication, DDC (duplication, degeneration,
complementation) model of evolution after duplication. These are the
original papers, but afterwards there have been hundreds of papers
published following these ideas:**

[***https://www.ncbi.nlm.nih.gov/pubmed/10101175***](https://www.ncbi.nlm.nih.gov/pubmed/10101175)

[***https://www.ncbi.nlm.nih.gov/pubmed/10629003***](https://www.ncbi.nlm.nih.gov/pubmed/10629003)

[***https://www.ncbi.nlm.nih.gov/pubmed/11779815***](https://www.ncbi.nlm.nih.gov/pubmed/11779815)

**3. Xenopus laevis WGD, a detailed study of "recent" evolutionary
events after duplication:**

[***http://www.nature.com/nature/journal/v538/n7625/abs/nature19840.html***](http://www.nature.com/nature/journal/v538/n7625/abs/nature19840.html)

**Amphioxus**

**2. Amphioxus genome papers:**

**2a. The first, B. floridae, together with a companion paper in Gen
Res:**

[***https://www.ncbi.nlm.nih.gov/pubmed/18563158***](https://www.ncbi.nlm.nih.gov/pubmed/18563158)

[***https://www.ncbi.nlm.nih.gov/pubmed/18562680***](https://www.ncbi.nlm.nih.gov/pubmed/18562680)

[](https://www.ncbi.nlm.nih.gov/pubmed/18562680)

**2b. The second, B. belcheri, the paper is not great, but there might
be things relevant for ours, including methylation:**

[***https://www.ncbi.nlm.nih.gov/pubmed/25523484***](https://www.ncbi.nlm.nih.gov/pubmed/25523484)

 To investigate the evolution and origins of vertebrate gene regulation,
appropriate species must be used for comparison. Previous studies have
largely focused on phylogenetic ranges that are either too short (e.g.
human vs. mouse) or too long (e.g. human vs. fly, human vs. nematode),
resulting in limited insights into the origins of vertebrate genome
regulation.

 In the first case, comparisons among closely related species (e.g.
mammals

 10 Berthelot, C., Villar, D., Horvath, J. E., Odom, D. T. & Flicek, P.
Complexity and conservation of regulatory landscapes underlie
evolutionary resilience of mammalian gene expression. Nat Ecol Evol 2,
152-163 (2018).

 11 Cotney, J. et al. The evolution of lineage-specific regulatory
activities in the human embryonic limb. Cell 154, 185-196 (2013).

 12 Reilly, S. K. et al. Evolutionary genomics. Evolutionary changes in
promoter and enhancer activity during human corticogenesis. Science 347,
1155-1159 (2015).

 13 Villar, D. et al. Enhancer evolution across 20 mammalian species.
Cell 160, 554-566 (2015).

 14 Stergachis, A. B. et al. Conservation of trans-acting circuitry
during mammalian regulatory evolution. Nature 515, 365-370 (2014).

 15 Vierstra, J. et al. Mouse regulatory DNA landscapes reveal global
principles of cis-regulatory evolution. Science 346, 1007-1012 (2014).

 16 Zhou, X. et al. Epigenetic modifications are associated with
inter-species gene expression variation in primates. Genome Biol 15, 547
(2014).

 or Drosophila species

 17 Bradley, R. K. et al. Binding site turnover produces pervasive
quantitative changes in transcription factor binding between closely
related Drosophila species. PLoS Biol 8, e1000343 (2010).

 18 He, Q. et al. High conservation of transcription factor binding and
evidence for combinatorial regulation across six Drosophila species. Nat
Genet 43, 414-420 (2011).

 19 Paris, M. et al. Extensive divergence of transcription factor
binding in Drosophila embryos with highly conserved gene expression.
PLoS Genet 9, e1003748 (2013).

 20 Khoueiry, P. et al. Uncoupling evolutionary changes in DNA sequence,
transcription factor occupancy and enhancer activity. Elife 6, pii:
e28440 (2017).

 for which orthology of non-coding regions can be readily determined
from genome alignments, have allowed fine-grained analyses of TF binding
evolution. These studies have revealed high rates of repurposing and TF
binding turnover

 11,12,15

 21 Odom, D. T. et al. Tissue-specific transcriptional regulation has
diverged significantly between human and mouse. Nat Genet 39, 730-732
(2007).

 despite high conservation of tissue-dependent expression

 22 Chan, E. T. et al. Conservation of core gene expression in
vertebrate tissues. J Biol 8, 33 (2009).

 23 Brawand, D. et al. The evolution of gene expression levels in
mammalian organs. Nature 478, 343-348 (2011).

 However, turnover rates and evolutionary conservation vary considerably
across tissues/cell types

 15

 24 Nord, A. S. et al. Rapid and pervasive changes in genome-wide
enhancer usage during mammalian development. Cell 155, 1521-1531 (2013).

 developmental stages 24

 and types and location of regulatory elements (i.e. promoters versus
enhancers, proximal versus distal)

 11-13,19,24

 oreover, it is unclear if similar evolutionary trends are present
outside mammals and flies and at larger evolutionary distances. In the
second case, three-way comparisons of human, fly and nematode by the
modENCODE consortium revealed no detectable conservation at the
cis-regulatory level

 25 Boyle, A. P. et al. Comparative analysis of regulatory information
and circuits across distant species. Nature 512, 453-456 (2014).

 and very little conservation of gene expression

 26 Gerstein, M. B. et al. Comparative analysis of the transcriptome
across distant species. Nature 512, 445-448 (2014).

 Moreover, a wealth of studies has demonstrated that the genomes of
fruitflies and nematodes are highly derived

 27 Hendrich, B. & Tweedie, S. The methyl-CpG binding domain and the
evolving role of DNA methylation in animals. Trends Genet 19, 269-277
(2003).

 28 Irimia, M. et al. Extensive conservation of ancient microsynteny
across metazoans due to cis-regulatory constraints. Genome Res 22,
2356-2367 (2012).

 29 Simakov, O. et al. Insights into bilaterian evolution from three
spiralian genomes. Nature 493, 526-531 (2013).

 Thus, comprehensive functional genomic data for a slow-evolving,
closely related outgroup is still missing for a proper investigation of
the origins and evolution of the vertebrate regulatory genome.
Furthermore, only by comparison between vertebrates and a closely
related outgroup will it be possible to elucidate the impact of WGDs on
gene regulation.

 Unlike flies, nematodes and most non-vertebrates, amphioxus belongs to
the chordate phylum. Therefore, although it lacks specializations and
innovations of vertebrates, it shares with them a basic body plan,
including a dorsal neural tube with an anterior brain, segmented
somites, notochord, and a ventral gut with a hepatic diverticulum
homologous to the vertebrate liver

 1

 For these reasons, amphioxus has been widely used as a reference
outgroup to infer ancestral versus novel features during vertebrate
evolution. Here, to investigate how the unique functional genome
architecture of vertebrates has evolved, we undertook a comprehensive
study of the transcriptome and regulatory genome of amphioxus.

Supp.I. 1:

1.1) History and phylogenetic position

1.2) Distribution, ecology and life cycle

4\. modENCODE (these are the 2 most relevant ones, but in the "news &
views" you have also links to the rest of modENCODE articles):

[*https://www.ncbi.nlm.nih.gov/pubmed/25164755*](https://www.ncbi.nlm.nih.gov/pubmed/25164755)

[*https://www.ncbi.nlm.nih.gov/pubmed/25164757*](https://www.ncbi.nlm.nih.gov/pubmed/25164757)

[*https://www.ncbi.nlm.nih.gov/pubmed/25164742*](https://www.ncbi.nlm.nih.gov/pubmed/25164742)

THE FIS TF
