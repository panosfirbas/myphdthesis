REFERENCES
1	Bertrand, S. & Escriva, H. Evolutionary crossroads in developmental biology: amphioxus. Development 138, 4819-4830 (2011).
2	Janvier, P. Facts and Fancies about Early Fossil Chordates and Vertebrates. Nature 520, 483-489 (2015).
3	Dehal, P. & Boore, J. L. Two rounds of whole genome duplication in the ancestral vertebrate. PLoS Biol 3, e314 (2005).
4	Putnam, N. et al. The amphioxus genome and the evolution of the chordate karyotype. Nature 453, 1064-1071 (2008).
5	Holland, L. Z. et al. The amphioxus genome illuminates vertebrate origins and cephalochordate biology. Genome Res 18, 1100-1111 (2008).
6	Lander, E. S. et al. Initial sequencing and analysis of the human genome. Nature 409, 860 - 921 (2001).
7	Nelson, C., Hersh, B. & Carroll, S. The regulatory content of intergenic DNA shapes genome architecture. Genome Biology 5, R25 (2004).
8	Vavouri, T. & Lehner, B. Conserved noncoding elements and the evolution of animal body plans. Bioessays 31, 727-735 (2009).
9	Bogdanović, O. et al. Active DNA demethylation at enhancers during the vertebrate phylotypic period. Nat Genet 48, 417-426 (2016).
10	Berthelot, C., Villar, D., Horvath, J. E., Odom, D. T. & Flicek, P. Complexity and conservation of regulatory landscapes underlie evolutionary resilience of mammalian gene expression. Nat Ecol Evol 2, 152-163 (2018).
11	Cotney, J. et al. The evolution of lineage-specific regulatory activities in the human embryonic limb. Cell 154, 185-196 (2013).
12	Reilly, S. K. et al. Evolutionary genomics. Evolutionary changes in promoter and enhancer activity during human corticogenesis. Science 347, 1155-1159 (2015).
13	Villar, D. et al. Enhancer evolution across 20 mammalian species. Cell 160, 554-566 (2015).
14	Stergachis, A. B. et al. Conservation of trans-acting circuitry during mammalian regulatory evolution. Nature 515, 365-370 (2014).
15	Vierstra, J. et al. Mouse regulatory DNA landscapes reveal global principles of cis-regulatory evolution. Science 346, 1007-1012 (2014).
16	Zhou, X. et al. Epigenetic modifications are associated with inter-species gene expression variation in primates. Genome Biol 15, 547 (2014).
17	Bradley, R. K. et al. Binding site turnover produces pervasive quantitative changes in transcription factor binding between closely related Drosophila species. PLoS Biol 8, e1000343 (2010).
18	He, Q. et al. High conservation of transcription factor binding and evidence for combinatorial regulation across six Drosophila species. Nat Genet 43, 414-420 (2011).
19	Paris, M. et al. Extensive divergence of transcription factor binding in Drosophila embryos with highly conserved gene expression. PLoS Genet 9, e1003748 (2013).
20	Khoueiry, P. et al. Uncoupling evolutionary changes in DNA sequence, transcription factor occupancy and enhancer activity. Elife 6, pii: e28440 (2017).
21	Odom, D. T. et al. Tissue-specific transcriptional regulation has diverged significantly between human and mouse. Nat Genet 39, 730-732 (2007).
22	Chan, E. T. et al. Conservation of core gene expression in vertebrate tissues. J Biol 8, 33 (2009).
23	Brawand, D. et al. The evolution of gene expression levels in mammalian organs. Nature 478, 343-348 (2011).
24	Nord, A. S. et al. Rapid and pervasive changes in genome-wide enhancer usage during mammalian development. Cell 155, 1521-1531 (2013).
25	Boyle, A. P. et al. Comparative analysis of regulatory information and circuits across distant species. Nature 512, 453-456 (2014).
26	Gerstein, M. B. et al. Comparative analysis of the transcriptome across distant species. Nature 512, 445-448 (2014).
27	Hendrich, B. & Tweedie, S. The methyl-CpG binding domain and the evolving role of DNA methylation in animals. Trends Genet 19, 269-277 (2003).
28	Irimia, M. et al. Extensive conservation of ancient microsynteny across metazoans due to cis-regulatory constraints. Genome Res 22, 2356-2367 (2012).
29	Simakov, O. et al. Insights into bilaterian evolution from three spiralian genomes. Nature 493, 526-531 (2013).
30	Muffato, M., Louis, A., Poisnel, C. E. & Crollius, H. R. Genomicus: a database and a browser to study gene synteny in modern and ancestral genomes. Bioinformatics 26, 1119-1121 (2010).
31	Celniker, S. E. et al. Unlocking the secrets of the genome. Nature 459, 927-930 (2009).
32	Nepal, C. et al. Dynamic regulation of the transcription initiation landscape at single nucleotide resolution during vertebrate embryogenesis. Genome Res 23, 1938-1950 (2013).
33	Forrest, A. R. et al. A promoter-level mammalian expression atlas. Nature 507, 462-470 (2014).
34	Schep, A. N. et al. Structured nucleosome fingerprints enable high-resolution mapping of chromatin architecture within regulatory regions. Genome Res 25, 1757-1770 (2015).
35	Wang, X. et al. Genome-wide and single-base resolution DNA methylomes of the Pacific oyster Crassostrea gigas provide insight into the evolution of invertebrate CpG methylation. BMC Genomics 15, 1119 (2014).
36	Albalat, R., Martí-Solans, J. & Cañestro, C. DNA methylation in amphioxus: from ancestral functions to new roles in vertebrates. Brief Funct Genomics 11, 142-155 (2012).
37	Huang, S. et al. Decelerated genome evolution in modern vertebrates revealed by analysis of multiple lancelet genomes. Nat Commun 5, 5896 (2014).
38	Odom, D. T. et al. Control of pancreas and liver gene expression by HNF transcription factors. Science 303, 1378-1381 (2004).
39	Aldea, D., Leon, A., Bertrand, E. & Escriva, H. Expression of Fox genes in the cephalochordate Branchiostoma lanceolatum. Front Ecol Evol 3, 80 (2015).
40	Zhang, Y. et al. Nucleation of DNA repair factors by FOXA1 links DNA demethylation to transcriptional pioneering. Nat Genet 48, 1003-1013 (2016).
41	Yang, Y. A. et al. FOXA1 potentiates lineage-specific enhancer activation through modulating TET1 expression and function. Nucleic Acids Res 44, 8153-8164 (2016).
42	Irie, N. & Kuratani, S. Comparative transcriptome analysis reveals vertebrate phylotypic period during organogenesis. Nat Commun 2, 248 (2011).
43	Hu, H. et al. Constrained vertebrate evolution by pleiotropic genes. Nat Ecol Evol, doi: 10.1038/s41559-41017-40318-41550 (2017).
44	Yanai, I. Development and Evolution through the Lens of Global Gene Regulation. Trends Genet, pii: S0168-9525(0117)30171-30173 (2017).
45	Duboule, D. Temporal colinearity and the phylotypic progression: a basis for the stability of a vertebrate Bauplan and the evolution of morphologies through heterochrony. Dev Suppl, 135-142 (1994).
46	Kumar, L. & Futschik, M. E. Mfuzz: A software package for soft clustering of microarray data. Bioinformatics 2, 5-7 (2007).
47	Yue, F. et al. A comparative encyclopedia of DNA elements in the mouse genome. Nature 515, 355-364 (2014).
48	Langfelder, P. & Horvath, S. WGCNA: an R package for weighted correlation network analysis. BMC Bioinformatics 9, 559 (2008).
49	McLean, C. Y. et al. GREAT improves functional interpretation of cis-regulatory regions. Nat Biotechnol 28, 495-501 (2010).
50	Force, A. et al. Preservation of duplicate genes by complementary, degenerative mutations. Genetics 151, 1531-1545 (1999).
51	Yanai, I. et al. Genome-wide midrange transcription profiles reveal expression level relationships in human tissue specification. Bioinformatics 21, 650-659 (2005).
52	Zemach, A., McDaniel, I. E., Silva, P. & Zilberman, D. Genome-wide evolutionary analysis of eukaryotic DNA methylation. Science 328, 916-919 (2010).
53	Feng, S. et al. Conservation and divergence of methylation patterning in plants and animals. Proc Natl Acad Sci USA 107, 8689-8694 (2010).
54	Schwaiger, M. et al. Evolutionary conservation of the eumetazoan gene regulatory landscape. Genome Res 24, 639-650 (2014).
55	Riviere, G. et al. Dynamics of DNA methylomes underlie oyster development. PLoS Genet 13, e1006807 (2017).
56	Maeso, I. & Tena, J. J. Favorable genomic environments for cis-regulatory evolution: A novel theoretical framework. Semin Cell Dev Biol 57, 2-10 (2016).
57	Darbellay, F. & Duboule, D. Topological Domains, Metagenes, and the Emergence of Pleiotropic Regulations at Hox Loci. Curr Top Dev Biol 116, 299-314 (2016).