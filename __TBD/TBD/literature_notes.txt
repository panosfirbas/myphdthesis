#fap_1

Amphioxus (also called lancelets or cephalochordates) form one of the three chordate subphyla, along with urochordates and vertebrates (Schubert et al., 2006). They form a small group comprising about 35 species (Poss and Boschung, 1996). The number of genera within the cephalochordate subphylum has long been debated, but recent molecular phylogenetic studies show that cephalochordates are divided into three genera (Kon et al., 2007) (Fig. 1B): Branchiostoma, Epigonichthys and Asymmetron.

notochord (see Glossary, Box 1), the defining morphological trait of chordates (Yarrell, 1836). 

the closest living relatives of vertebrates, with urochordates representing the most basally divergent chordate lineage. These

However, in 2006, based on large molecular data set analyses, it was established that cephalochordates represent the most basally divergent lineage of chordates, being the sister group of urochordates and vertebrates (Bourlat et al., 2006; Delsuc et al., 2006).This new phylogenetic framework has important implications because it shows that: (1) urochordates are extremely derived animals; and (2) the ancestor of chordates was probably an amphioxus-like animal, as various fossils suggest (Holland and Chen, 2001).
The

The adult anatomy of amphioxus is vertebrate-like, but simpler. Amphioxus possess typical chordate characters, such as a dorsal hollow neural tube and notochord, a ventral gut and a perforated pharynx with gill slits, segmented axial muscles and gonads, a post- anal tail, a pronephric kidney, and homologues of the thyroid gland and adenohypophysis (the endostyle and pre-oral pit, respectively) (Fig. 2A). However, they lack typical vertebrate-specific structures, such as paired sensory organs (image-forming eyes or ears), paired appendages, neural crest cells and placodes (see Glossary, Box 1) (Schubert et al., 2006). 

These two genome duplications have been convincingly supported by the complete genome sequencing of several chordate species (fap_3).

Moreover, the ‘pre-duplicated’
amphioxus genome possesses one
representative of almost all the members of all the gene families that presumably existed in the ancestor of chordates, in contrast to the situation in the two other chordate subphyla, urochordates and vertebrates, which have specifically lost different members of these gene families (e.g. the homeobox, tyrosine kinase or nuclear receptor families) (Bertrand et al., 2011a; D’Aniello et al., 2008; Takatori et al., 2008)

Amphioxus embryogenesis was first described by Kowalevsky (Kowalevsky, 1867), who divided it into an early phase of development resembling that of invertebrate deuterostomes (i.e. a hollow blastula invaginates to form a gastrula in a similar manner to the sea urchin) and a later phase that is vertebrate-like (with the formation of a notochord, a dorsal hollow nerve chord, segmented axial muscles, etc.) (Fig. 3).

The free-living planktonic larvae show an asymmetric body plan: the mouth forms on the left side, the gill slits form on the right-ventral side and the right series of somites form half a segment posterior to the left ones. During metamorphosis, which occurs 2-3 weeks to 2-3 months after fertilization, depending on the species, much of this asymmetry disappears, although the axial muscles retain their asymmetry. An interesting difference between Branchiostoma and Asymmetron species is that while Branchiostoma adults show two rows of symmetric gonads ventrolaterally, Asymmetron develops gonads on only the right side of the body.    
    
# Nice pictures (Fig 3) of amphi development

The demonstration of the 2R hypothesis finally came from analyses using complete genome sequences of species throughout the chordate lineage (fap_3), and was corroborated when the first amphioxus genome sequence became available (Putnam et al., 2008). At this point, there is still an unanswered question concerning the exact timing of such genome duplications within the vertebrate evolutionary history. Did they occur before the split between cyclostomes (see Glossary, Box 1) and gnathostomes (see Glossary, Box 1), or did one duplication occur before and one after this evolutionary branchpoint (Fig. 1C)? The answer to this question cannot be obtained using uniquely phylogenetic approaches (Escriva et al., 2002b; Kuraku et al., 2009) and awaits the publication of complete cyclostome genome sequences.
Hox


# Amphioxus as a model has helped provide insights into 


Evolution of the chordate genome, Hox clusters and the evolution of cis-regulatory elements, Axial patterning,
Evolution of vertebrate structures, The head, Migratory neural crest cells, Placodes, Bones, Paired appendages, the immune system, metamorphosis


# fap_2
Living vertebrates fall into two major clades, the cyclostomes (hagfishes
and lampreys) and the gnathostomes (jawed vertebrates). Only the lat- ter produce bone and dentine.

# fap_3

It has long been hypothesized that the increased complex-
ity and genome size of vertebrates has resulted from two rounds (2R) of whole genome duplication (WGD) occurring in early vertebrate evolution, thus providing the requisite raw materials [1]. 
Conflicting analyses have now made this very controversial, with some studies supporting 2R (e.g., [4–8]), others seeing only a single round of WGD (e.g., [9–11]), and still others refuting WGD altogether by concluding that nothing greater than limited segmental duplications have occurred (e.g., [12,13])

the conclusions could never be viewed as definitively resolving this issue because these products could have alternatively been generated by duplications of individual genes or short gene segments
rather than by WGDs. Even duplicating all of the genes in a genome individually is quite different from a whole genome duplicating simultaneously. There are several reasons why this has been a difficult issue
to resolve. After duplication, only the minority of gene pairs will adopt a new function (‘‘neofunctionalization’’)or partition old functions (‘‘subfunctionalization’’)quickly enough to escape disabling mutations that would lead to their eradication [24]; therefore, rampant gene loss rapidly erases this signal of genome duplication. Further, four- member gene families, even those with the (AB)(CD) top- ology, can be generated by two rounds of duplications of individual genes or of segments much smaller than the entire genome, generating a condition that cannot be differentiated on this basis from 2R followed by many gene losses. This alternative scenario seems especially plausible because recent analyses have shown that gene duplications occur much more frequently than had been thought, with the typical rate being sufficient to duplicate an entire genome equivalent every 100 million years (MY) [25,26]. 

Fortunately, as has been shown convincingly for the yeast genome and for Arabidopsis [27–30], evidence of an ancient genome duplication can be seen in the large-scale pattern of the physical locations of homologous genes, even when the great majority of the duplicated genes have been lost. Studies

# Great Fig1 on WGD

An early observation in support of 2R was that several gene families have expanded from a single member in inverte- brates to having four members for some vertebrates. Previous studies, confirmed in this analysis, have shown that this is not generally true for vertebrate multigene families [12].

Although the 4-fold (i.e., including the query segment) category is the most prevalent, it accounts for only 25% of the genome. Nonetheless, it is striking that this remains the largest category despite approximately 450 MY of evolution. This constitutes a strong signal of 2R, and could not reasonably have been generated by a series of smaller duplication events. For


#fap_4

Lancelets, or amphioxus, are small worm-like marine animals that spend most oftheir lives buried in the sea floor, filter-feeding through jawless, ciliated mouths. The vertebrate affinities of these modest creatures were first noted in the early part of the nineteenth cen- tury1,2
, and were further clarified by the embryologist Alexander
Kowalevsky3
. In particular, Kowalevsky observed that, unlike other
invertebrates, amphioxus shares key anatomical and developmental features with vertebrates and tunicates (also known as urochordates). These include a hollow dorsal neural tube, a notochord, a perforated pharyngeal region, a segmented body musculature (embryologically derived from somites) and a post-anal tail. Together, the vertebrates, urochordates and lancelets (also known as cephalochordates) con- stitute the phylum Chordata, descended from a last common ancestor that lived perhaps 550 million years ago.



# fap_5

Roots of the vertebrate endocrine system 
Pituitary control of reproduction—A vertebrate innovation
Analysis
Components of the stress response
An ancient route for thyroid hormone production in amphioxus
Insulin family members,Nuclear receptors
Adaptive and innate immunity

Conserved noncoding sequences

# fap_6
Initial sequencing and analysis of the human genome


# fap_7 The regulatory content of intergenic DNA shapes genome architecture
To investigate this, we examined the relationship between regulatory complexity and gene spacing in Caenorhabditis elegans and Drosophila melanogaster. We found that gene density directly reflects local regulatory complexity, such that the amount of noncoding DNA between a gene and its nearest neighbors correlates positively with that gene's regulatory complexity.

<NICE> Genes with complex functions are flanked by significantly more noncoding DNA than genes with simple or housekeeping functions.

while genes with common housekeeping functions occupy approximately the same amount of space in both D. melanogaster and C. elegans, genes that play a central role in development and pattern formation occupy significantly more space in D. melanogaster. Finally,


# fap_8
CNEs are not distributed evenly throughout vertebrate
genomes but cluster around genes that encode transcription factors and other regulatory genes(10–12,22) (Fig.

While other ‘‘by-stander’’ genes are normally lost from a duplicated region containing a CNE, the CNE almost invariably remains in cis with a developmental regulatory gene.

	# from_fap_8
	Over two thirds of the families are conserved in duplicate in fish and appear to predate the large-scale duplication events thought to have occurred at the origin of vertebrates. We propose a model whereby gene duplication and the evolution of cis-regulatory elements can be considered in the context of increased morphological diversity and the emergence of the modern vertebrate body plan.

	disruption of the proximity of CRMs from their target gene via chromosomal breakpoints has been shown to cause congenital disease in a number of cases (Kleinjan and van Heyningen 2005).

	
