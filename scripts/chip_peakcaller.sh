#!/bin/bash
#
#SBATCH --job-name=atacmap
#SBATCH --output=slurm-atacmapp-%j.o.out
#SBATCH --error=slurm-atacmapp-%j.e.out
#
#SBATCH --ntasks=1
#
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2



fq=${1}
basen=$(basename $1)
name=${basen%.rmdup.bam}

gsize=${2}

from=${SLURM_SUBMIT_DIR}



macs2 callpeak -f BAM -g ${gsize} -t ${fq} -n ${name} --outdir ${from} 
# macs2 callpeak --broad -f BAM -g ${gsize} -t ${fq} -n broad_${name} --outdir ${from}


