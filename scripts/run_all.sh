for nb in ./*ipynb;
do
 echo doing $nb
 jupyter nbconvert --inplace --to notebook --execute $nb
done
