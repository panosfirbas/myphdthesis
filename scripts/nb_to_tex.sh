
nb=$1
name=$(basename $nb )
name=${name%.ipynb}


echo "converting" ${name}" to tex"

# PANDOC needs to be installed in the system for this to work
# I had to slightly manipulate the template to bring it to the thesis' measurements
jupyter nbconvert --template=$(pwd)/other/LatexTemplates/classicm.tplx --to=latex --output=$(pwd)/other/NotebookToPDFStaging/${name}.tex $nb