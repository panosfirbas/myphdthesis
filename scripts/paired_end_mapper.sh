#!/bin/bash
#
#SBATCH --job-name=atacmap
#SBATCH --output=slurm-atacmapp-%j.o.out
#SBATCH --error=slurm-atacmapp-%j.e.out
#
#SBATCH --ntasks=1
#
#SBATCH --nodes=1
#SBATCH --cpus-per-task=12



export BOWTIE2_INDEXES='/scratch/indexes/bowtie2/'

samtools13=/home/ska/tools/samtools1.3/samtools

fq=${1}

fq_1=${fq}_1.fq
fq_2=${fq}_2.fq

name=$(basename $1)

index=${2}

from=${SLURM_SUBMIT_DIR}

TMPDIR='/scratch/panos_q'${SLURM_JOB_ID}'/'
mkdir $TMPDIR


temp_bam=${TMPDIR}${name}.bam
final_bam=${from}/${name}.rmdup.bam

bowtie2 \
-p 12 \
-x ${index} \
-1 ${fq_1} \
-2 ${fq_2} \
--dovetail \
-I 0 \
-X 2000 \
--very-sensitive 2> ${from}/${name}_bowtie2.log | \
${samtools13} view -Shu - > ${temp_bam}

${samtools13} sort -@ 11 -T ${TMPDIR}/temp ${temp_bam} | \
${samtools13} rmdup -s - - > ${final_bam}
${samtools13} index ${final_bam}

rm ${temp_bam}

rm -r ${TMPDIR}
