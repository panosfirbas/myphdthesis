nb=$1
name=$(basename $nb )
name=${name%.ipynb}

echo "running " ${nb}
jupyter nbconvert --ExecutePreprocessor.timeout=none --inplace --to notebook --execute $nb
#echo "converting to html"
#jupyter nbconvert --to=html --output=../VC/Appendices/${name}.html $nb
