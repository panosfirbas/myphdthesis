
GENOMES - Repeat Masks - Annotations
	zebra
		danRer10
		danRer11
	amphi
		bl71nemr
	medaka
		orylat2
	mouse
		mm10

BASAL - GREAT - INTERGENIC regions
	




CHIP
	zebra
		[x] fastqs
		[x] qual check
		[x] mapping
		[x] mapping reports
		peak calling
			[x] naive peaks [ broad/narrow, rep/combo ]
			[ ] idr

	amphi
		[x] fastqs
		[x] qual check
		[x] mapping
		[x] mapping reports
		peak calling
			[x] naive peaks [ broad/narrow, rep/combo ]
			[ ] idr


ATAC
	zebra
		[x] fastqs
		[x] qual check
		[x] mapping
		[x] mapping reports
		[x] nf_reads

		peak calling
			[x] naive peaks
			[x] idr
	amphi
		[x] fastqs
		[x] qual check
		[x] mapping
		[x] mapping reports
		[x] nf_reads

		peak calling
			[x] naive peaks
			[x] idr
	medaka
		[x] fastqs
		[x] qual check
		[x] mapping
		[x] mapping reports
		[x] nf_reads

		peak calling
			[x] naive peaks
			[x] idr


	< !! > The following have no replicates:

			/home/ska/panos/Thesis/data/ATAC/amphi/nf_reads/ATAC_amphioxus_muscle_rep1_nf_reads_*.bed.gz
			/home/ska/panos/Thesis/data/ATAC/amphi/nf_reads/ATAC_amphioxus_ntube_rep1_nf_reads_*.bed.gz
			/home/ska/panos/Thesis/data/ATAC/medaka/nf_reads/ATAC_medaka_80epi_rep1_nf_reads_*.bed.gz
			/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_fin_rep1_nf_reads_*.bed.gz

RNA
	cRPKMS



