

# run the notebooks in place, this produces the proper out cells
########################################################
# bash comp_run_notebooks.sh



# nbconvert the notebooks to .tex files
########################################################
# bash comp_tex_the_notebooks.sh



# compile the texes into pdfs
########################################################
# bash comp_pdf_the_notebooks.sh



# Bibliography refresh
########################################################
cat ./other/mendeley_bibtex_sync/library.bib ./other/mendeley_bibtex_sync/manual_bibtex/*.bib > ./VC/MyThesisBibliography.bib
grep -v '^url' ./VC/MyThesisBibliography.bib > temp && mv temp ./VC/MyThesisBibliography.bib
grep -v '^arXiv' ./VC/MyThesisBibliography.bib > temp && mv temp ./VC/MyThesisBibliography.bib



# Compile the book pdf
########################################################
cd VC
rm main*.aux main*.blg main*.log main*.out main*.pdf main*.toc main*.docx

# convert to a bad word-like file
# pandoc main.tex --bibliography=MyThesisBibliography.bib -o main.docx

# xelatex main.tex
# bibtex main.aux
# xelatex main.tex

xelatex main_serif.tex
bibtex main_serif.aux
xelatex main_serif.tex

# xelatex main_sans.tex
# bibtex main_sans.aux
# xelatex main_sans.tex




echo '###################### "Undefined" grep in '
echo '###################### "serif'
grep undefined main_serif.log
# echo '###################### "sans'
# grep undefined main_sans.log

# cp ./main.pdf ../
cd ..




# try this is preamble
# pd.set_option('display.latex.repr', True)
# pd.set_option('display.latex.longtable', True)