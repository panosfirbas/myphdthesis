# compile the texes into pdfs
########################################################

cp ./other_notebooks/hybrid.png . #hacky


cd ./other/NotebookToPDFStaging/
rm *.aux *.blg *.log *.out *.pdf *.toc *.docx
cd ../../


for tex in ./other/NotebookToPDFStaging/*tex;
do
	echo $tex
	# this fixes a paths issue
	sed -i "s#/home/ska/panos/myphdthesis/#/#" $tex
	# /home/ska/panos/myphdthesis/
	pdflatex -output-directory=./other/NotebookToPDFStaging/ $tex
done

rm ./hybrid.png #hacky

cp ./other/NotebookToPDFStaging/*pdf ./VC/Appendices
########################################################